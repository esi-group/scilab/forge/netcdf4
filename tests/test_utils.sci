origfuncprot = funcprot();
funcprot(0);

format(15);

libscinetcdf_Init();

scinetcdf_path = getSciNetCDFPath();
exec(fullfile(scinetcdf_path, "tests/cdl_gen.sci"));

function assert_checkreturn(return_code, expected_return_code, func, msg)
    if ~isequal(return_code, expected_return_code) then
        errmsg = msprintf("%s returns %d while expected is %d", func, return_code, expected_return_code);
        if exists("msg") then
            disp(msg);
        end
        assert_generror(errmsg);
    end
endfunction

function filepath = getncfilename()
    tmpname = tempname();
    filepath = fullfile(tmpname, "test.nc");
    deletefile(tmpname);
    mkdir(tmpname);
endfunction

function cdl_content = clean_cdl_content(cdl_content)
    for i=1:size(cdl_content, 'r')
        idx = strindex(cdl_content(i), "//");
        if idx <> [] then
            cdl_content(i) = part(cdl_content(i), 1:idx-1);
        end
    end;
    cdl_content = stripblanks(cdl_content, %t);
    cdl_content = cdl_content(find(cdl_content <> ""));
endfunction

function check_cdl_content(cdl_content, expected_cdl_content)
    cdl_content = clean_cdl_content(cdl_content);
    expected_cdl_content = clean_cdl_content(expected_cdl_content);
    if find(cdl_content <> expected_cdl_content) <> [] then
        disp("result CDL content:");
        disp(cdl_content);
        disp("expected CDL content:");
        disp(expected_cdl_content);
        assert_generror("assertion failed");
    end
endfunction

function new_value = convert_to(type_name, varname)
    if type_name <> "float" then
        new_value = eval(msprintf("%s(%s)", type_name, varname));
    else
        new_value = eval(varname);
    end
endfunction

function [subs, subs_dims] = build_subs(dims, start, count, stride)
    nbdims = max(size(find(dims > 1), 'c'), 1);
    if nbdims == 1 then
        dims = max(dims);
    end
    if ~isdef('start') | (start == []) then
        start = ones(nbdims, 1);
    end
    if ~isdef('count') | (count == []) then
        count = repmat(%inf, 1, nbdims);
    end
    if ~isdef('stride') | (stride == []) then
        stride = ones(nbdims, 1);
    end
    subs_dims = [];
    subs_ranges = list();
    for i=1:nbdims
        stop = min(start(i)+(count(i)-1)*stride(i), dims(i));
        subs_ranges(i) = start(i):stride(i):stop;
        subs_dims(i) = size(subs_ranges(i), '*');
    end

    nb_subs = prod(subs_dims);
    counters = ones(nbdims, 1);
    subs = [];
    i = 1;
    while i <= nb_subs
        for j=1:nbdims
            subs(i, j) = subs_ranges(j)(counters(j));
        end
        i = i + 1;
        j = 1;
        while %t
            counters(j) = counters(j) + 1;
            if j >= nbdims then
                break;
            end
            if counters(j) <= subs_dims(j) then
                break;
            end
            j = j + 1;
        end
        for k=1:j-1
            counters(k) = 1;
        end
    end
endfunction

// NetCDF low level functions are suffixed by the type name except for char which suffix is 'text"
function suffix = lowlevel_function_suffix(typeName)
    if typeName <> "char" then
        suffix = typeName;
    else
        suffix = "text";
    end
endfunction

// test data
dims1 = list("cols1", 3);
dims2 = list("rows2", 4, "cols2", 3);
dims3 = list("rows3", 4, "cols3", 3, "mats3", 2);
dims = list(dims1, dims2, dims3);

data1 = [1 2 3];
data2 = [1 2 3; 3 4 5; 5 6 7; 8 9 10];
data3 = hypermat([3, 4, 2], 1:24);
data3 = permute(data3, [2 1 3]);
data = list(data1, data2, data3);

dimsInf1 = list("infcols1", %inf);
dimsInf2 = list("infrows2", %inf, "infcols2", 3);
dimsInf3 = list("infrows3", 4, "infcols3", 3, "infmats3", %inf);
dimsInf = list(dimsInf1, dimsInf2, dimsInf3);

dataInf1 = [1 2 3];
dataInf2 = [1 2 3; 3 4 5; 5 6 7; 8 9 10];
dataInf3 = hypermat([3, 4, 2], 1:24);
dataInf3 = permute(dataInf3, [2 1 3]);
dataInf = list(dataInf1, dataInf2, dataInf3);

num_types = list("int8", "uint8", "int16", "uint16", "int32", "uint32", "float", "double");
types = lstcat(num_types, list("char"));


funcprot(origfuncprot);
