function sz = getsize(st)
    if typeof(st) == "st" then
        sz = max(definedfields(st))-2;
    else
        sz = max(definedfields(st))/2;
    end
endfunction

function nctypename = getnctypename(type_name)
    if type_name == "double" then
        nctypename = "double";
    elseif type_name == "int8" then
        nctypename = "byte";
    elseif type_name == "uint8" then
        nctypename = "ubyte";
    elseif type_name == "int16" then
        nctypename = "short";
    elseif type_name == "uint16" then
        nctypename = "ushort";
    elseif type_name == "int32" then
        nctypename = "int";
    elseif type_name == "uint32" then
        nctypename = "uint";
    elseif type_name == "float" then
        nctypename = "float";
    elseif type_name == "string" then
        nctypename = "char";
    elseif type_name == "char" then
        nctypename = "char";
    else
        nctypename = "double";
    end
endfunction

function attvalue = getattvalue(st_var, attname)
    attvalue = [];
    if isfield(st_var, "atts") then
        atts = st_var.atts;
        for i=1:size(atts)
            st_att = atts(i);
            if isfield(st_att, "name") then
                if st_att.name == attname then
                    if isfield(st_att, "value") then
                        attvalue = st_att.value;
                        return;
                    end
                end
            end
        end
    end
endfunction

// Put dimensions in the expected NetCDF dimensions order
function st_nc_dims = put_dims_to_nc_order(st_dims)
    nbdims = getsize(st_dims);
    if nbdims > 1
        st_nc_dims = list();
        for i=0:nbdims-3
            st_nc_dims(2*i+1) = getfield(2*(nbdims-i)-1, st_dims);
            st_nc_dims(2*i+2) = getfield(2*(nbdims-i), st_dims);
        end
        st_nc_dims(2*nbdims-3) = getfield(1, st_dims);
        st_nc_dims(2*nbdims-2) = getfield(2, st_dims);
        st_nc_dims(2*nbdims-1) = getfield(3, st_dims);
        st_nc_dims(2*nbdims) = getfield(4, st_dims);
    else
        st_nc_dims = st_dims;
    end
endfunction

function cdl_dim_content = gen_cdl_dim(st_dims, idx)
    dimname = getfield(2*idx+1, st_dims);
    dimsize = getfield(2*idx+2, st_dims);
    if ~isinf(dimsize)
        sdimsize = string(dimsize);
    else
        sdimsize = "UNLIMITED";
    end
    cdl_dim_content = dimname + " = " + sdimsize + " ;\n";
endfunction

function cdl_dims_content = gen_cdl_dims(st_params)
     cdl_dims_content = [];
    if isfield(st_params, "dimensions") & ~isempty(st_params.dimensions) then
        cdl_dims_content = "dimensions:\n";
        st_dims = st_params.dimensions;
        nbdims = getsize(st_dims);
        for i=0:nbdims-1
            cdl_dims_content = cdl_dims_content + gen_cdl_dim(st_dims, i);
        end
    end
endfunction

function cdl_att_content = gen_cdl_att(st_att, parent)
    if isfield(st_att, "name") then
        // if variable attribute, prefix with variable name
        if isfield(parent, "name") & isfield(parent, "type") then
            cdl_att_content = parent.name + ":" + st_att.name + " = ";
        else
            cdl_att_content = ":" + st_att.name + " = ";
        end

        if isfield(st_att, "type") then
            atttype = st_att.type;
        else
            atttype = "double";
        end

        str_att_value = [];
        for i=1:size(st_att.value, '*')
            str_att_value = str_att_value + string(st_att.value(i));

            if atttype == "double" then
                if isempty(strchr(string(st_att.value(i)), ".")) then
                    str_att_value = str_att_value + ".";
                end
            elseif atttype == "float" then
                if isempty(strchr(string(st_att.value(i)), ".")) then
                    str_att_value = str_att_value + ".f";
                end
            elseif atttype == "uint" then
                str_att_value = str_att_value + "U";
            elseif atttype == "short" then
                str_att_value = str_att_value + "s";
            elseif atttype == "ushort" then
                str_att_value = str_att_value + "US";
            elseif atttype == "byte" then
                str_att_value = str_att_value + "b";
            elseif atttype == "ubyte" then
                str_att_value = str_att_value + "UB";
            elseif atttype == "char" then
                str_att_value = """" + str_att_value + """";
            end

            if i<size(st_att.value, '*') then
                str_att_value = str_att_value + ","
            end
        end

        cdl_att_content = cdl_att_content + str_att_value + " ;\n";
    else
       cdl_att_content = [];
    end
endfunction

function cdl_atts_content = gen_cdl_atts(st_params)
    cdl_atts_content = [];
    if isfield(st_params, "atts") then
        atts = st_params.atts;
        for i=1:size(atts)
            cdl_atts_content = cdl_atts_content + gen_cdl_att(atts(i), st_params);
        end
    end
endfunction

function cdl_var_content = gen_cdl_var(st_var)
    cdl_var_content = [];
    if ~isfield(st_var, "name") | (st_var.name == "") then
        return;
    end
    if isfield(st_var, "type") then
        stype = getnctypename(st_var.type);
    end

    cdl_var_content = stype + " " + st_var.name;
    if isfield(st_var, "dimensions") then
        cdl_var_content = cdl_var_content + "(";
        st_var_dims = st_var.dimensions;
        nbdims = getsize(st_var_dims);
        for i=0:nbdims-1
            dimname = getfield(2*i+1, st_var_dims);
            cdl_var_content = cdl_var_content + dimname;
            if i<nbdims-1 then
                cdl_var_content = cdl_var_content + ", ";
            end
        end
        cdl_var_content = cdl_var_content + ")";
    end
    cdl_var_content = cdl_var_content + " ;\n";
    cdl_var_content = cdl_var_content + gen_cdl_atts(st_var);
endfunction

function cdl_vars_content = gen_cdl_vars(st_params)
    if isfield(st_params, "variables") then
        cdl_vars_content = "variables:\n";
        st_vars = st_params.variables;
        for i=1:size(st_vars)
            cdl_vars_content = cdl_vars_content + gen_cdl_var(st_vars(i))
        end
    else
        cdl_vars_content = [];
    end
endfunction

function cdl_group_content = gen_cdl_group(st_group)
    if isfield(st_group, "name") then
        cdl_group_content = "group: " + st_group.name + " {\n" + ..
            + gen_cdl_atts(st_group) + ..
            + gen_cdl_dims(st_group) + ..
            + gen_cdl_groups(st_group) + ..
            + gen_cdl_vars(st_group) + ..
            + gen_cdl_data(st_group) + ..
            + "}\n";
    else
        cdl_group_content = [];
    end
endfunction

function cdl_groups_content = gen_cdl_groups(st_params)
    if isfield(st_params, "groups") then
        st_groups = st_params.groups;
        cdl_groups_content = [];
        for i=1:size(st_groups)
            cdl_groups_content = cdl_groups_content + gen_cdl_group(st_groups(i))
        end
    else
       cdl_groups_content = [];
    end
endfunction

function cdl_value_content = gen_cdl_data_value(value, st_var)
    fillvalue = getattvalue(st_var, "_FillValue");

    if isfield(st_var, "type") then
        if st_var.type == "char" then
            cdl_value_content = """" + value + """";
            return;
        end
    end

    if ~isnan(value) then
        if value <> fillvalue then
            cdl_value_content = string(value);
            return;
        end
    end

    cdl_value_content = "_";

    // Bug in ncdump ? It displays "_" for default fill values for all types,
    // except for bytes ("-127") and ubytes ("255")
    if fillvalue == [] then
        if isfield(st_var, "type") then
            if st_var.type == "int8" then
                cdl_value_content = "-127";
            elseif st_var.type == "uint8" then
                cdl_value_content = "255";
            end
        end
    end
endfunction

function cdl_data_value_content = gen_cdl_data_values(st_data, st_var)
    cdl_data_value_content = [];
    if ~isfield(st_data, "data") then
        return;
    end
    data = st_data.data;
    if isfield(st_var, "dimensions")
        st_var_dims = st_var("dimensions");
    else
        st_var_dims = [];
    end
    if ~isempty(st_var_dims) then
        // calculate size of variable data
        var_nbdims = getsize(st_var_dims);
        var_dims = [];
        var_size = 1;
        for i=1:var_nbdims
            var_dims($+1) = getfield(2*i, st_var_dims);
            var_size = var_size * var_dims($);
        end
        data_size = size(data, '*');
        if var_size == %inf then
            var_size = data_size;
        end

        if var_nbdims > 1 then
            // NetCDF dims are reversed
            for i=1:var_nbdims
                var_dims_tmp(i) = var_dims(var_nbdims-i+1);
            end
            var_dims = var_dims_tmp;

            data_dims = size(data);

            data_nbdims = size(data_dims, 'c');
            if data_nbdims > 1 then
                data = permute(data, [2 1 3:data_nbdims]);
            end

            cdl_data_value_content = cdl_data_value_content + "\n";
        else
            data_dims = data_size;
        end

        if isfield(st_data, "indexs")
            data_idxs = st_data("indexs");
        else
            data_idxs = ind2sub(data_dims, 1:data_size);
        end
        if size(data_idxs, 'c') > 1 then
            data_idxs(:, 1:2) = [data_idxs(:,2), data_idxs(:,1)];
        end

        var_idxs = ones(var_nbdims, 1);
        data_idx = 1;
        var_count = 1;
        while (var_count <= var_size)
            if find(var_idxs > var_dims) == [] then
                if vectorfind(data_idxs, var_idxs) == [] then
                    value = gen_cdl_data_value(%nan, st_var);
                else
                    value = gen_cdl_data_value(data(data_idx), st_var);
                    data_idx = data_idx + 1;
                end
                if var_count < var_size then
                    cdl_data_value_content = cdl_data_value_content + value + ", ";
                else
                    cdl_data_value_content = cdl_data_value_content + value + " ;";
                end
                var_count = var_count + 1;
            end
            i = 1;
            while (%t)
                var_idxs(i) = var_idxs(i) + 1;
                if i >= var_nbdims then
                    break;
                end
                if var_idxs(i) <= var_dims(i) then
                    break;
                end
                i = i + 1;
            end
            if i <> 1 then
                cdl_data_value_content = cdl_data_value_content + "\n";
            end
            var_idxs(1:i-1, 1) = ones(i-1, 1);
        end
    else
        cdl_data_value_content = gen_cdl_data_value(data) + " ;";
    end
endfunction

function cdl_data_content = gen_cdl_data(st_params)
    if isfield(st_params, "data") then
        cdl_data_content = "data:\n";
        datas = st_params.data;
        nbdata = size(datas);
        if isfield(st_params, "variables") then
            variables = st_params("variables");
            for i=1:nbdata
                st_data = datas(i);
                // TODO : search variable by name
                st_var = variables(i);
                cdl_vardata = gen_cdl_data_values(st_data, st_var);
                varname = st_data.name;
                cdl_data_content = cdl_data_content + varname + " = ";
                cdl_data_content = cdl_data_content + cdl_vardata + "\n";
            end
        end
    else
        cdl_data_content = [];
    end
endfunction

function cdl_content = gen_cdl(st_params)
    cdl_content = msprintf("netcdf test {\n" ..
        + gen_cdl_atts(st_params) ..
        + gen_cdl_dims(st_params) ..
        + gen_cdl_vars(st_params) ..
        + gen_cdl_groups(st_params) ..
        + gen_cdl_data(st_params) ..
        + "}\n");
endfunction

// extract varname and groups (for now manages 2 groups max in depth)
function [st_var, groups] = extract_var_and_group(varname, st_params)
    st_var = struct();
    groups = list(st_params);

    if isempty(varname) | (varname == "/") then
        return;
    end

    nodes = strsplit(varname, "/");

    // varname
    if ~isempty(nodes($)) then
        st_var = struct("name", nodes($));
        nodes($) = "";
    end

    // extract groups (2 groups max in depth)
    nodes = nodes(find(nodes <> ""));
    nbnodes = size(nodes, 'r');
    if nbnodes > 0 then
        groups(2) = struct("name", nodes(1));
        if nbnodes > 1 then
            groups(3) = struct("name", nodes(2));
        end
    end
endfunction

function cdl_content = gen_cdl_oneatt(varname, attname, attvalue)
    [st_var, groups] = extract_var_and_group(varname, struct());

    atttype = getnctypename(typeof(attvalue));
    st_att = struct("name", attname, "type", atttype, "value", attvalue);

    // affect attributes and variables
    if isfield(st_var, "name") then
        st_var("type") = "double";
        st_var("atts") = list(st_att);
        groups($)("variables") = list(st_var);
    else
        groups($)("atts") = list(st_att);
    end

    // chain groups
    for i=size(groups):-1:2
        groups(i-1)("groups") = list(groups(i));
    end

    cdl_content = gen_cdl(groups(1));
endfunction

function cdl_content = gen_cdl_onevar(vartype, varname, vardata, st_params)
    [st_var, groups] = extract_var_and_group(varname, st_params);

    st_var("type") = vartype;

    nbgroups = size(groups);

    // dimensions
    if isfield(st_params, "dimensions") then
        st_dims = put_dims_to_nc_order(st_params.dimensions);
        st_var("dimensions") = st_dims;
        groups(nbgroups)("dimensions") = st_dims;
        if nbgroups > 1 then
            groups(1).dimensions = [];
        end
    end
    // fill value
    if isfield(st_params, "fillvalue") then
        if st_params.fillvalue <> "disable" then
            nctype = getnctypename(vartype);
            st_att = struct("name", "_FillValue", "type", nctype, "value", st_params.fillvalue);
            st_var("atts") = list(st_att);
        end
    end

    // affect variables to group
    groups(nbgroups)("variables") = list(st_var);

    // data
    if ~isempty(vardata) then
        st_data = struct("name", st_var.name, "data", vardata);

        if isfield(st_params, "data_indexs") then
            st_data("indexs") = st_params.data_indexs;
        end

        groups(nbgroups)("data") = list(st_data);
    end

    // chain groups
    for i=nbgroups:-1:2
        groups(i-1)("groups") = list(groups(i));
    end

    cdl_content = gen_cdl(groups(1));
endfunction

function create_ncfile_from_cdl(filepath, cdl_content)
    cdl_filepath = tempname();
    fd = mopen(cdl_filepath, "wt");
    mputl(cdl_content, fd);
    mclose(fd);

    ncgen(cdl_filepath, filepath);

    deletefile(cdl_filepath);
endfunction

function create_ncfile_oneatt(filepath, varname, attname, attvalue)
    cdl_content = gen_cdl_oneatt(varname, attname, attvalue);
    create_ncfile_from_cdl(filepath, cdl_content);
endfunction

function create_ncfile_onevar(filepath, vartype, varname, vardata, st_params)
    cdl_content = gen_cdl_onevar(vartype, varname, vardata, st_params);
    create_ncfile_from_cdl(filepath, cdl_content);
endfunction


