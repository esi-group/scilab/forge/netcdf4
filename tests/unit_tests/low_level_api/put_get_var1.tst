scinetcdf_path = getSciNetCDFPath();
exec(fullfile(scinetcdf_path, "tests/test_utils.sci"));

function filepath = test_put_get_var1_type(typeName, typeId, values)
  filepath = fullfile(TMPDIR, msprintf("put_get_var1_%s.nc", typeName));
  [ret, ncid] = nc_create(filepath, NC_NETCDF4 + NC_CLOBBER);
  assert_checkreturn(ret, 0, "nc_create(...)");

  if typeName <> "char" then
    dims = size(values);
    nbDims = max(size(find(dims > 1), 'c'), 1);
  else
    dims = length(values);
    nbDims = 1;
  end

  dimids = [];
  for i=1:nbDims
    [ret, dimid] = nc_def_dim(ncid, msprintf("dim%d", i), dims(i));
    assert_checkreturn(ret, 0, "nc_def_dim(...)");
    dimids = [dimids, dimid];
  end

  [ret, varid] = nc_def_var(ncid, "var", typeId, dimids);
  assert_checkreturn(ret, 0, "nc_def_var(...)");

  nbValues = prod(dims);

  if typeName <> "char" then
    subs = ind2sub(dims, 1:nbValues);
  end

  for i = 1:nbValues
    if typeName <> "char" then
      sub = subs(i, :);
      value = values(sub2ind(dims, sub));
      cmd = msprintf("ret = nc_put_var1_%s(ncid, varid, sub-1, value)", typeName);
    else
      value = part(values, i:i);
      cmd = "ret = nc_put_var1_text(ncid, varid, i-1, value)";
    end
    execstr(cmd);
    assert_checkreturn(ret, 0, cmd);
  end

  ret = nc_close(ncid);
  assert_checkreturn(ret, 0, "nc_close(...)");

  [ret, ncid] = nc_open(filepath, NC_NOWRITE);
  assert_checkreturn(ret, 0, "nc_open(...)");

  [ret, varid] = nc_inq_varid(ncid, "var");
  assert_checkreturn(ret, 0, "nc_inq_varid(...)");

  for i = 1:nbValues
    if typeName <> "char" then
      sub = subs(i, :);
      expectedValue = values(sub2ind(dims, sub));
      cmd = msprintf("[ret, value] = nc_get_var1_%s(ncid, varid, sub-1)", typeName);
    else
      cmd = "[ret, value] = nc_get_var1_text(ncid, varid, i-1)";
      expectedValue = part(values, i:i);
    end
    execstr(cmd);
    assert_checkequal(value, expectedValue);
  end

  ret = nc_close(ncid);
  assert_checkreturn(ret, 0, "nc_close(...)");
endfunction

value = [1 2; 3 4];
test_put_get_var1_type("schar", NC_BYTE, value);
test_put_get_var1_type("uchar", NC_UBYTE, value);
test_put_get_var1_type("short", NC_SHORT, value);
test_put_get_var1_type("ushort", NC_USHORT, value);
test_put_get_var1_type("int", NC_INT, value);
test_put_get_var1_type("uint", NC_UINT, value);
test_put_get_var1_type("uchar", NC_FLOAT, value);
test_put_get_var1_type("double", NC_DOUBLE, value);

test_put_get_var1_type("char", NC_CHAR, "abcd");
test_put_get_var1_type("string", NC_STRING, ["aa" "bb"; "cc" "dd"]);

