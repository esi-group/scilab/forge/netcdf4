exec loader.sce

function assert_checkreturn(return_code, expected_return_code, func)
    if ~isequal(return_code, expected_return_code) then
        assert_generror(msprintf("%s returns %d, expected return is %d", func, ..
          return_code, expected_return_code));
    end
endfunction


OLD_NAME = "Constantinople";
NEW_NAME = "Istanbul";
CONTENTS = "Lots of people!";

[ret, ncid] = nc_create("tst_atts.nc", NC_NETCDF4 + NC_CLOBBER);
assert_checkreturn(ret, 0, "nc_create(...)");

ret = nc_put_att_text(ncid, NC_GLOBAL, OLD_NAME, length(CONTENTS)+1, CONTENTS);
assert_checkreturn(ret, 0, "nc_put_att_text");

ret = nc_rename_att(ncid, NC_GLOBAL, OLD_NAME, NEW_NAME);
assert_checkreturn(ret, 0, "nc_rename_att(...)");

[ret, attid] = nc_inq_attid(ncid, NC_GLOBAL, NEW_NAME);
assert_checkreturn(ret, 0, "nc_inq_attid(...) (first)");

[ret, att_text] = nc_get_att_text(ncid, NC_GLOBAL, NEW_NAME);
assert_checkreturn(ret, 0, "nc_get_att_text(...) (first)");
assert_checkequal(CONTENTS, att_text);

ret = nc_close(ncid);
assert_checkreturn(ret, 0, "nc_close(...) (first)");

[ret, ncid] = nc_open("tst_atts.nc", NC_NOWRITE);
assert_checkreturn(ret, 0, "nc_open(...)");

[ret, attid] = nc_inq_attid(ncid, NC_GLOBAL, NEW_NAME);
assert_checkreturn(ret, 0, "nc_inq_attid(...) (second)");
assert_checkequal(attid, 0);

[ret, att_text] = nc_get_att_text(ncid, NC_GLOBAL, NEW_NAME);
assert_checkreturn(ret, 0, "nc_get_att_text(...) (first)");
assert_checkequal(CONTENTS, att_text);

ret = nc_close(ncid);
assert_checkreturn(ret, 0, "nc_close(...) (second)");

deletefile("tst_atts.nc");
