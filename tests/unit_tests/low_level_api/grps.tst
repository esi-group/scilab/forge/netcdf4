scinetcdf_path = getSciNetCDFPath();
exec(fullfile(scinetcdf_path, "tests/test_utils.sci"));

filepath = fullfile(TMPDIR, "tst_grps.nc");

[ret, ncid] = nc_create(filepath, NC_NETCDF4 + NC_CLOBBER);
assert_checkreturn(ret, 0, "nc_create(...)");

[ret, grp1id] = nc_def_grp(ncid, "grp1");
assert_checkreturn(ret, 0, "nc_def_grp(ncid, ""grp1"")");

[ret, grp1name] = nc_inq_grpname(grp1id);
assert_checkreturn(ret, 0, "nc_inq_grpname(...)");

[ret, grp2id] = nc_def_grp(ncid, "grp2");
assert_checkreturn(ret, 0, "nc_def_grp(ncid, ""grp2"")");

[ret, grpids] = nc_inq_grps(ncid);
assert_checkequal(grpids, [65537 65538]);

[ret, subgrp1id] = nc_def_grp(grp1id, "subgrp1");
assert_checkreturn(ret, 0, "nc_def_grp(ncid, ""grp2"")");

[ret, subgrp1name] = nc_inq_grpname(subgrp1id);
assert_checkreturn(ret, 0, "nc_inq_grpname(subgrp1id)");
assert_checkequal(subgrp1name, "subgrp1");

[ret, subgrp1fullname] = nc_inq_grpname_full(subgrp1id);
assert_checkreturn(ret, 0, "nc_inq_fullgrpname(subgrp1id)");
assert_checkequal(subgrp1fullname, "/grp1/subgrp1");

[ret, parentgrp1id] = nc_inq_grp_parent(subgrp1id);
assert_checkreturn(ret, 0, "nc_inq_fullgrpname(subgrp1id)");
assert_checkequal(parentgrp1id, 65537);

ret = nc_close(ncid);
assert_checkreturn(ret, 0, "nc_close(...)");

deletefile(filepath);
