scinetcdf_path = getSciNetCDFPath();
exec(fullfile(scinetcdf_path, "tests/test_utils.sci"));

filepath = fullfile(scinetcdf_path, "tests/unit_tests/data/nc_inq.nc");

[ret, ncid] = nc_open(filepath, NC_NOWRITE);
assert_checkreturn(ret, 0, "nc_open(...)");

var1id = 0;

[ret, var1name, var1type, var1dimids, var1natts] = nc_inq_var(ncid, var1id);
assert_checkreturn(ret, 0, "nc_inq_var(...)");
assert_checkequal(var1name, "var1");
assert_checkequal(int32(var1type), NC_DOUBLE);
assert_checkequal(var1dimids, 0);
assert_checkequal(var1natts, 2);

[ret, var1name] = nc_inq_varname(ncid, var1id);
assert_checkreturn(ret, 0, "nc_inq_varname(...)");
assert_checkequal(var1name, "var1");

[ret, var1id] = nc_inq_varid(ncid, "var1");
assert_checkreturn(ret, 0, "nc_inq_varid(...)");
assert_checkequal(var1id, var1id);

[ret, var1type] = nc_inq_vartype(ncid, var1id);
assert_checkreturn(ret, 0, "nc_inq_vartype(...)");
assert_checkequal(int32(var1type), NC_DOUBLE);

[ret, var1ndims] = nc_inq_varndims(ncid, var1id);
assert_checkreturn(ret, 0, "nc_inq_varndims(..., var1id)");
assert_checkequal(var1ndims, 1);

var1dimids = new_IntArray(var1ndims);
ret = nc_inq_vardimid(ncid, var1id, var1dimids);
assert_checkreturn(ret, 0, "nc_inq_vardimid(..., var1id)");
assert_checkequal(IntArray_getitem(var1dimids, 0), 0);
delete_IntArray(var1dimids);

[ret, var1natts] = nc_inq_varnatts(ncid, var1id);
assert_checkreturn(ret, 0, "nc_inq_varnatts(...)");
assert_checkequal(var1natts, 2);

var2id = 1;

[ret, var2ndims] = nc_inq_varndims(ncid, var2id);
assert_checkreturn(ret, 0, "nc_inq_varndims(..., var2id)");
assert_checkequal(var2ndims, 2);

var2dimids = new_IntArray(var2ndims);
ret = nc_inq_vardimid(ncid, var2id, var2dimids);
assert_checkreturn(ret, 0, "nc_inq_vardimid(..., var2id)");
assert_checkequal(IntArray_getitem(var2dimids, 0), 1);
assert_checkequal(IntArray_getitem(var2dimids, 1), 2);
delete_IntArray(var2dimids);

ret = nc_close(ncid);
assert_checkreturn(ret, 0, "nc_close(...)");

