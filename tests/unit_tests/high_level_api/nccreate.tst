// <-- ENGLISH IMPOSED -->

scinetcdf_path = getSciNetCDFPath();
exec(fullfile(scinetcdf_path, "tests/test_utils.sci"));

function check_nccreate(varname, varargin)
    filepath = [];
    vartype = "double";
    cmd = "nccreate(filepath, varname";
    [lhs, rhs] = argn(0);
    if rhs > 1 then
        st_params = varargin(1);
        if isfield(st_params, "dimensions")
            cmd = cmd + ", ""Dimensions"", st_params.dimensions";
        end
        if isfield(st_params, "datatype")
            vartype = st_params.datatype;
            cmd = cmd + ", ""Datatype"", vartype";
        end
        if isfield(st_params, "fillvalue")
            cmd = cmd + ", ""FillValue"", st_params.fillvalue";
        end
        if rhs > 2 then
            filepath = varargin(2);
        end
    else
        st_params = struct();
    end
    if filepath == [] then
        filepath = getncfilename();
    end

    // Check nccreate() success
    cmd = cmd + ")";
    ierr = execstr(cmd, "errcatch");
    assert_checkreturn(ierr, 0, cmd, lasterror());

    // Check created file content
    cdl_content = ncdisp(filepath, '/', ["min", "to_var"]);
    expected_cdl_content = gen_cdl_onevar(vartype, varname, [], st_params);
    check_cdl_content(cdl_content, expected_cdl_content);
endfunction

check_nccreate("v");

// Groups
check_nccreate("a/v");
check_nccreate("grp/var1");
check_nccreate("grp/var2");
check_nccreate("a/b/var1");

// Slashs
check_nccreate("/var");
check_nccreate("/grp/var");
check_nccreate("/grp//var");

// Dimensions
for i=1:size(dims)
    check_nccreate("var", struct("dimensions", dims(i)));
end
for i=1:size(dimsInf)
    check_nccreate("var", struct("dimensions", dimsInf(i)));
end

// Dimensions-groups
check_nccreate("grp/var", struct("dimensions", dims(1)));

// Datatypes
for i=1:size(types)
    check_nccreate("var", struct("datatype", types(i)));
end

// Fill value
for i=1:size(types)
    if types(i) <> "char" then
        fill_value = convert_to(types(i), "0");
    else
        fill_value = "*";
    end
    check_nccreate("var", struct("datatype", types(i), "fillvalue", fill_value));
end
check_nccreate("var", struct("fillvalue", "disable"));

// Dimensions, datatype, and fill value
for i=1:size(types)
    for j=1:size(dims)
        if types(i) <> "char" then
            fill_value = convert_to(types(i), "0");
        else
            fill_value = "*";
        end
        params = struct("dimensions", dims(j), "datatype", types(i), "fillvalue", fill_value);
        check_nccreate("var", params);
    end
end

// Check invalid agument errors
expected_errmsg = "nccreate: Optional arguments are pairs of a name and a value.";
assert_checkerror("nccreate(getncfilename(), ""var"", ""Datatype"")", expected_errmsg);
expected_errmsg = "nccreate: Unknown type ''Foo'', supported types are ''double|float|int32|uint32|int16|uint16|int8|uint8|char''.";
assert_checkerror("nccreate(getncfilename(), ""var"", ""Datatype"", ""Foo"")", expected_errmsg);
expected_errmsg = "nccreate: 64 bit types are not supported in Scilab 5.";
assert_checkerror("nccreate(getncfilename(), ""var"", ""Datatype"", ""int64"")", expected_errmsg);

// Check error on creating the same variable twice
expected_errmsg = "nccreate: nc_def_var() returned the error -42: NetCDF: String match to name in use.";
filepath = getncfilename();
assert_checkerror("nccreate(filepath, ""var""); nccreate(filepath, ""var"")", expected_errmsg);
assert_checkerror("nccreate(filepath, ""grp/var""); nccreate(filepath, ""grp/var"")", expected_errmsg);

// Check error on using two differents specifications of the same dimension
expected_errmsg = "nccreate: The dimension ''dim'' already exists, but with a different size.";
filepath = getncfilename();
assert_checkerror("nccreate(filepath, ""var1"", ""Dimensions"", list(""dim"", 5)); nccreate(filepath, ""var2"", ""Dimensions"", list(""dim"", 10));", expected_errmsg);









