scinetcdf_path = getSciNetCDFPath();
exec(fullfile(scinetcdf_path, "tests/test_utils.sci"));

function check_ncwriteatt(location, attname, attvalue, varargin)
    [lhs, rhs] = argn(0);
    if rhs > 3 then
        filepath = varargin(1);
    else
        filepath = getncfilename();
    end

    create_ncfile_oneatt(filepath, location, attname, attvalue);

    // check ncwriteatt() return code
    cmd = "ncwriteatt(filepath, location, attname, attvalue)";
    ierr = execstr(cmd, "errcatch");
    assert_checkreturn(ierr, 0, cmd, lasterror());

    // TODO Check content
endfunction

// Groups attributes
check_ncwriteatt("/", "att", 3.2);
check_ncwriteatt("/grp/", "att", 3.2);
check_ncwriteatt("/grp/subgrp/subgrp2", "att", 3.2);

// Variable attributes
check_ncwriteatt("/var", "att", 2.5);
check_ncwriteatt("/grp/var", "att", 2.5);
check_ncwriteatt("/grp/subgrp/var", "att", 2.5);

// Datatypes
for i=1:size(num_types)
    attvalue = convert_to(types(i), "10");
    check_ncwriteatt("/", "att", attvalue);
end
// Char
check_ncwriteatt("/", "att", "hello");

// Vector
for i=1:size(num_types)
    attvalue = convert_to(types(i), "[1 2 3]");
    check_ncwriteatt("/", "att", attvalue);
end

// Check overwrite
filepath = getncfilename();
check_ncwriteatt("/", "pi", 3.14, filepath);
check_ncwriteatt("/", "pi", 3.14159, filepath);




