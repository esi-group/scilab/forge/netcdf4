scinetcdf_path = getSciNetCDFPath();
exec(fullfile(scinetcdf_path, "tests/test_utils.sci"));

function check_ncwrite_var(vartype, varname, vardata, varargin)
    overwrite = %f;
    [lhs, rhs] = argn(0);
    if rhs > 3 then
        st_params = varargin(1);
    else
        st_params = struct();
    end
    if rhs > 4 then
        start = varargin(2);
    else
        start = [];
    end
    if rhs > 5 then
        stride = varargin(3);
    else
        stride = [];
    end
    if rhs > 6 then
        filepath = varargin(4);
    else
        filepath = getncfilename();
    end
    if rhs > 7 then
        overwrite = varargin(5);
    else
        overwrite = %f;
    end

    // if no overwrite, create file with given variable
    if ~overwrite then
        create_ncfile_onevar(filepath, vartype, varname, [], st_params);
    end

    if start <> [] then
        [subdata_idxs, subdata_dims] = build_subs(size(vardata), start, [], stride)
        st_params("data_indexs") = subdata_idxs;

        // sub2ind needs 2D (row, col) indexs, does not support 1D indexs for vectors
        if size(subdata_idxs, 'c') == 1 then
            subdata_idxs = [ones(size(subdata_idxs, 'r'), 1) subdata_idxs];
        end

        idxs = sub2ind(size(vardata), subdata_idxs);
        subdata = vardata(unique(idxs));
        if size(subdata_dims, 'r') > 1 then
            subdata = hypermat(subdata_dims, subdata);
        end
    else
        subdata = vardata;
    end

    // check ncwrite() success
    cmd = "ncwrite(filepath, varname, subdata";
    if start <> [] then
        cmd = cmd + ", start";
        if stride <> [] then
            cmd = cmd + ", stride";
        end
    end
    cmd = cmd + ");"

    ierr = execstr(cmd, "errcatch");
    assert_checkreturn(ierr, 0, cmd, lasterror());

    // check content written
    cdl_content = ncdisp(filepath, "/", "to_var");
    expected_cdl_content = gen_cdl_onevar(vartype, varname, subdata, st_params);
    check_cdl_content(cdl_content, expected_cdl_content);
endfunction

// Scalar
check_ncwrite_var("double", "var", 3.2);

// Dimensions
for i=1:size(dims)
    check_ncwrite_var("double", "var", data(i), struct("dimensions", dims(i)));
end

// Unlimited dimension
for i=1:size(dimsInf)
    check_ncwrite_var("double", "var", dataInf(i), struct("dimensions", dimsInf(i)));
end

// Datatypes
for i=1:size(num_types)
    typed_data2 = convert_to(types(i), "data2");
    check_ncwrite_var(types(i), "var", typed_data2, struct("dimensions", dims2));
end

// Column vector
check_ncwrite_var("double", "var", data1', struct("dimensions", dims1));
check_ncwrite_var("double", "var", data1', struct("dimensions", dimsInf1));
for i=1:size(num_types)
    typed_data1 = convert_to(types(i), "data1");
    check_ncwrite_var("double", "var", typed_data1', struct("dimensions", dimsInf1));
end


// Char datatype
check_ncwrite_var("char", "var", "hello", struct("dimensions", dimsInf(1)));

// Groups
check_ncwrite_var("double", "grp/var", 1.0);
check_ncwrite_var("double", "/grp/subgrp/var", 2.0);

// Fill value
unfilled_data = [1 2; 4 5];
typed_unfilled_data = list();
for i=1:size(num_types)
    typed_unfilled_data(i) = convert_to(types(i), "unfilled_data");
end
// Default fill value
for i=1:size(num_types)
    st_params = struct("dimensions", dims2);
    check_ncwrite_var(types(i), "var", typed_unfilled_data(i), st_params);
end
// Specified fill value
for i=1:size(num_types)
    fill_value = convert_to(types(i), "2");
    st_params = struct("dimensions", dims2, "fillvalue", fill_value);
    check_ncwrite_var(types(i), "var", typed_unfilled_data(i), st_params);
end
// No fill value
for i=1:size(num_types)
    st_params = struct("dimensions", dims2, "fillvalue", "disable");
    check_ncwrite_var(types(i), "var", typed_unfilled_data(i), st_params);
end
// Nan management
unfilled_data = [%nan 2 3; 4 %nan 6; 7 %nan 9; 10 11 %nan];
st_params = struct("dimensions", dims2);
check_ncwrite_var("double", "var", unfilled_data, st_params);
st_params = struct("dimensions", dims2, "fillvalue", 2);
check_ncwrite_var("double", "var", unfilled_data, st_params);
st_params = struct("dimensions", dims2, "fillvalue", "disable");
check_ncwrite_var("double", "var", unfilled_data, st_params);

// Start & stride
for i=1:size(dims)
    st_params = struct("dimensions", dims(i));
    start = repmat(2, 1, i);
    check_ncwrite_var("double", "var", data(i), st_params, start);
end
for i=1:size(dims)
    st_params = struct("dimensions", dims(i));
    start = ones(1, i);
    stride = repmat(2, 1, i);
    check_ncwrite_var("double", "var", data(i), st_params, start, stride);
end

// Overwrite
filepath = getncfilename();
check_ncwrite_var("double", "pi", 3.14, struct(), [], [], filepath);
check_ncwrite_var("double", "pi", 3.14159, struct(), [], [], filepath, %t);


