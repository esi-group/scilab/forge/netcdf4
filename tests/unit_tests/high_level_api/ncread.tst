// <-- ENGLISH IMPOSED -->

scinetcdf_path = getSciNetCDFPath();
exec(fullfile(scinetcdf_path, "tests/test_utils.sci"));

function filepath = check_ncread_var(vartype, varname, vardata, varargin)
    [lhs, rhs] = argn(0);
    if rhs > 3 then
        st_params = varargin(1);
    else
        st_params = struct();
    end
    if rhs > 4 then
        start = varargin(2);
    else
        start = [];
    end
    if rhs > 5 then
        count = varargin(3);
    else
        count = [];
    end
    if rhs > 6 then
        stride = varargin(4);
    else
        stride = [];
    end
    if rhs > 7 then
        expected_vardata = varargin(5);
    else
        expected_vardata = vardata;
    end
    if rhs > 8 then
        filepath = varargin(6);
    else
        filepath = getncfilename();
    end

    if start <> [] then
        [subdata_idxs, subdata_dims] = build_subs(size(vardata), start, count, stride);
        nbdims = size(subdata_dims, 'r');
        st_params("data_indexs") = subdata_idxs;
        if nbdims > 1 then
            subdata_idxs = sub2ind(size(vardata), subdata_idxs);
        end
        expected_vardata = vardata(unique(subdata_idxs));
        if nbdims > 1 then
            expected_vardata = hypermat(subdata_dims, expected_vardata);
        end
    end

    if expected_vardata == [] then
        expected_vardata = vardata;
    end

    create_ncfile_onevar(filepath, vartype, varname, expected_vardata, st_params);

    // check ncread() success
    cmd = "vardata = ncread(filepath, varname";
    if start <> [] then
        cmd = cmd + ", start";
        if count <> [] then
            cmd = cmd + ", count";
            if stride <> [] then
                cmd = cmd + ", stride";
            end
        end
    end
    cmd = cmd + ");"

    ierr = execstr(cmd, "errcatch");
    assert_checkreturn(ierr, 0, cmd, lasterror());

    // for 2D hypermat() returns a matrix and not an hypermat,
    // need to convert test result to matrix
    if (type(expected_vardata) == 1) & (type(vardata) == 17) then
        vardata = vardata(:,:,1);
    end

    assert_checkequal(vardata, expected_vardata);
endfunction


// Scalar
check_ncread_var("double", "var", 3.2);

// Dimensions
for i=1:size(dims)
    check_ncread_var("double", "var", data(i), struct("dimensions", dims(i)));
end

// Unlimited dimension
for i=1:size(dimsInf)
    check_ncread_var("double", "var", dataInf(i), struct("dimensions", dimsInf(i)));
end

// Datatypes
for i=1:size(num_types)
    typed_data = convert_to(types(i), "data2");
    check_ncread_var(types(i), "var", typed_data, struct("dimensions", dims2));
end
// Char
check_ncread_var("char", "var", "Hello", struct("dimensions", dimsInf(1)));

// Groups
check_ncread_var("double", "grp/var", 1.0);
check_ncread_var("double", "/grp/subgrp/var", 2.0);

// Fill value
// Tested for all numeric types, except float bea
unfilled_data = list();
for i=1:size(num_types)
    unfilled_data(i) = convert_to(types(i), "[1 2; 4 5]");
end
// Default fill value
expected_unfilled_data = list(int8([1 2 -127; 4 5 -127; 7 8 -127; 10 11 -127]), ..
    uint8([1 2 255; 4 5 255; 7 8 255; 10 11 255]), ..
    int16([1 2 -32767; 4 5 -32767; 7 8 -32767; 10 11 -32767]), ..
    uint16([1 2 65535; 4 5 65535; 7 8 65535; 10 11 65535]), ..
    int32([1 2 -2147483647; 4 5 -2147483647; 7 8 -2147483647; 10 11 -2147483647]), ..
    uint32([1 2 4294967295; 4 5 4294967295; 7 8 4294967295; 10 11 4294967295]), ..
    [1 2 %nan; 4 5 %nan; 7 8 %nan; 10 11 %nan], ..
    [1 2 %nan; 4 5 %nan; 7 8 %nan; 10 11 %nan]);
for i=1:size(num_types)
    check_ncread_var(types(i), "var", unfilled_data(i), struct("dimensions", dims2), [], [], [], expected_unfilled_data(i));
end
// Specified fill value
expected_unfilled_data = list();
for i=1:size(num_types)-2
    expected_unfilled_data(i) = convert_to(types(i), "[1 2 2; 4 5 2; 7 2 9; 2 11 12]");
end
expected_unfilled_data($+1) =  [1 %nan %nan; 4 5 %nan; 7 %nan 9; %nan 11 12];
expected_unfilled_data($+1) =  [1 %nan %nan; 4 5 %nan; 7 %nan 9; %nan 11 12];
for i=1:size(num_types)
    fill_value = convert_to(types(i), "2");
    st_params = struct("dimensions", dims2, "fillvalue", fill_value);
    check_ncread_var(types(i), "var", unfilled_data(i), st_params, [], [], [], expected_unfilled_data(i));
end

// Start, count & stride
for i=1:size(dims)
    st_params = struct("dimensions", dims(i));
    start = ones(1, i);
    check_ncread_var("double", "var", data(i), st_params, start);
end
for i=1:size(dims)
    st_params = struct("dimensions", dims(i));
    start = ones(1, i);
    count = repmat(2, 1, i);
    check_ncread_var("double", "var"+string(i), data(i), st_params, start, count);
end
for i=1:size(dims)
    st_params = struct("dimensions", dims(i));
    start = ones(1, i);
    count = ones(1, i);
    stride = repmat(2, 1, i);
    check_ncread_var("double", "var", data(i), st_params, start, count, stride);
end

// Check errors
// File not found
expected_errmsg = "ncread: nc_open() returned the error 2: No such file or directory.";
filepath = getncfilename();
assert_checkerror("ncread(""notfound.nc"", ""var"");", expected_errmsg);

// Variable not found
filepath = getncfilename();
nccreate(filepath, "var");
expected_errmsg = "ncread: nc_inq_varid() returned the error -49: NetCDF: Variable not found.";
assert_checkerror("ncread(filepath, ""notfound"");", expected_errmsg);

