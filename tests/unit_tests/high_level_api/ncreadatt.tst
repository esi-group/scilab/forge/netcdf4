scinetcdf_path = getSciNetCDFPath();
exec(fullfile(scinetcdf_path, "tests/test_utils.sci"));

function check_ncreadatt(location, attname, attvalue, varargin)
    [lhs, rhs] = argn(0);
    if rhs > 3 then
        filepath = varargin(1);
    else
        filepath = getncfilename();
    end

    create_ncfile_oneatt(filepath, location, attname, attvalue);

    expected_attvalue = attvalue;
    // check ncreadatt() return code
    cmd = "attvalue = ncreadatt(filepath, location, attname)";
    ierr = execstr(cmd, "errcatch");
    assert_checkreturn(ierr, 0, cmd, lasterror());

    // check attribute value
    assert_checkequal(attvalue, expected_attvalue);
endfunction

// Vector
for i=1:size(num_types)
    attvalue = convert_to(types(i), "[1 2 3]");
    check_ncreadatt("/", "var", attvalue);
end

// Groups attributes
check_ncreadatt("/", "att", 3.2);
check_ncreadatt("/grp/", "att", 3.2);
check_ncreadatt("/grp/subgrp/", "att", 3.2);

// Variable attributes
check_ncreadatt("/var", "att", 2.5);
check_ncreadatt("/grp/var", "att", 2.5);
check_ncreadatt("/grp/subgrp/var", "att", 2.5);

// Datatypes
for i=1:size(num_types)
    attvalue = convert_to(types(i), "10");
    check_ncreadatt("/", "att", attvalue);
end
// Char
check_ncreadatt("/", "att", "hello");

// Vector
for i=1:size(num_types)
    attvalue = convert_to(types(i), "[1 2 3]");
    check_ncreadatt("/", "var", attvalue);
end

// Check overwrite
filepath = getncfilename();
check_ncreadatt("/", "pi", 3.14, filepath);
check_ncreadatt("/", "pi", 3.14159, filepath);





