// Test for issue 1592
// http://forge.scilab.org/index.php/p/netcdf4/issues/1592/

scinetcdf_path = getSciNetCDFPath();
exec(fullfile(scinetcdf_path, "tests/cdl_gen.sci"));

filepath = fullfile(TMPDIR, "test 1.nc");
nccreate(filepath, "var");
ncdisp(filepath);

deletefile(filepath);
