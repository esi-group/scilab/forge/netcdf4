scinetcdf_path = getSciNetCDFPath();
exec(fullfile(scinetcdf_path, "tests/test_utils.sci"));

NDIMS = 2;
NX = 6;
NY = 12;
SIZE = NX * NY;

filepath = fullfile(TMPDIR, "simple_nc4.nc");
deletefile(filepath);

[ret, ncid] = nc_create(filepath, NC_NETCDF4 + NC_CLOBBER);
assert_checkreturn(ret, 0, "nc_create(...)");

[ret, x_dimid] = nc_def_dim(ncid, "x", NX);
assert_checkreturn(ret, 0, "nc_def_dim(ncid, ""x"", NX)");
[ret, y_dimid] = nc_def_dim(ncid, "y", NY);
assert_checkreturn(ret, 0, "nc_def_dim(ncid, ""y"", NY)");

dimids = [x_dimid, y_dimid];

[ret, grp1id] = nc_def_grp(ncid, "grp1");
assert_checkreturn(ret, 0, "nc_def_grp(ncid, ""grp1"")");

[ret, var1id] = nc_def_var(grp1id, "data", NC_INT, dimids);
assert_checkreturn(ret, 0, "nc_def_var(grp1id, ...)");

dataIn = new_IntArray(SIZE);
for i=0:SIZE-1
    IntArray_setitem(dataIn, i, i);
end
ret = nc_put_var_int(grp1id, var1id, dataIn);
assert_checkreturn(ret, 0, "nc_put_var_int(...)");
delete_IntArray(dataIn);

[ret, grp2id] = nc_def_grp(ncid, "grp2");
assert_checkreturn(ret, 0, "nc_def_grp(ncid, ""grp2"")");

[ret, typeid] = nc_def_compound(grp2id, 8, "sample_compound_type");
assert_checkreturn(ret, 0, "nc_def_compound(...)");

ret = nc_insert_compound(grp2id, typeid, "i1", 0, NC_INT);
assert_checkreturn(ret, 0, "nc_insert_compound(..., ""i1"", ...)");

ret = nc_insert_compound(grp2id, typeid, "i2", 4, NC_INT);
assert_checkreturn(ret, 0, "nc_insert_compound(..., ""i2"", ...)");

[ret, var2id] = nc_def_var(grp2id, "data", typeid, dimids);
assert_checkreturn(ret, 0, "nc_def_var(grp2id, ...)");

compound_data = new_IntArray(2*SIZE);
for i=0:2*SIZE-1
    if modulo(i, 2) == 0 then
        IntArray_setitem(compound_data, i, 42);
    else
        IntArray_setitem(compound_data, i, -42);
    end
end
ret = nc_put_var(grp2id, var2id, compound_data);
delete_IntArray(compound_data);

ret = nc_close(ncid);
assert_checkreturn(ret, 0, "nc_close(...)");
