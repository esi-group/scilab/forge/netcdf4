<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2015 - Scilab Enterprises
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry xmlns="http://docbook.org/ns/docbook"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:ns3="http://www.w3.org/1999/xhtml" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab"
  xml:lang="en"
  xml:id="ncdisp">
    <refnamediv>
        <refname>ncdisp</refname>
        <refpurpose>Outputs the content of a NetCDF source.</refpurpose>
    </refnamediv>
    <refsynopsisdiv>
        <title>Calling Sequence</title>
        <synopsis>output = ncdisp(source, location, [output_options])</synopsis>
    </refsynopsisdiv>
    <refsection>
        <title>Arguments</title>
        <variablelist>
            <varlistentry>
                <term>source</term>
                <listitem><para>Path to a NetCDF file or URL of an OpenNDAP source (string).</para></listitem>
            </varlistentry>
            <varlistentry>
                <term>location</term>
                <listitem><para>Optional path of a group or variable (string). By defaut is '/' (data of all variables are output). For a group, the path must end with a '/'.</para></listitem>
            </varlistentry>
            <varlistentry>
                <term>output_options</term>
                <listitem><para>Optional parameters to control the output (matrix of string):
                    <listitem><para>'min': the data of variables are not displayed, only the definitions.</para></listitem>
                    <listitem><para>'to_var': the content is returned into <term>output</term> instead of the console.</para></listitem>
                </para></listitem>
            </varlistentry>
        </variablelist>
    </refsection>
    <refsection>
        <title>Description</title>
        <para><function>ncdisp</function> displays the content of a NetCDF <term>source</term> (NetCDF file or OpenNDAP source).</para>

        <para>It displays the same content as the NetCDF <function>ncdump</function> tool. The definitions and data of variables (and all other content: group hierarchy, dimensions,...) are displayed in the console using the CDL notation (Common Data Language).</para>

        <para>By default the data for all variables are output.
        The <term>location</term> parameter is used as a filter to output data only for a specific variable or group, which can be convenient if the NetCDF source is big.</para>
    </refsection>
    <refsection>
        <title>Examples</title>
        <programlisting role="example">
        <![CDATA[
        scinetcdf_path = getSciNetCDFPath();
        source = fullfile(scinetcdf_path, 'tests/unit_tests/data/micro.nc');

        // Displays the whole content of the file
        ncdisp(source);

        // Displays only the definitions of variables (no data)
        ncdisp(source, '/', 'min');

        // Displays all the definitions and the data of the variable 'x'
        ncdisp(source, 'x');

        // Displays all the definitions and the data of the group 'grp'
        ncdisp(source, 'grp/');
        ]]>
        </programlisting>
    </refsection>
    <refsection role="see also">
        <title>See Also</title>
        <simplelist type="inline">
            <member><ulink url="https://www.unidata.ucar.edu/software/netcdf/docs/netcdf/ncdump.html">ncdump</ulink></member>
        </simplelist>
    </refsection>
</refentry>
