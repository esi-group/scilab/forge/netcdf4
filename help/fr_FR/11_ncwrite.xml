<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2015 - Scilab Enterprises
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry xmlns="http://docbook.org/ns/docbook"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:ns3="http://www.w3.org/1999/xhtml" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab"
  xml:lang="fr"
  xml:id="ncwrite">
    <refnamediv>
        <refname>ncwrite</refname>
        <refpurpose>Ecrit une variable dans un fichier NetCDF.</refpurpose>
    </refnamediv>
    <refsynopsisdiv>
        <title>Séquence d'appel</title>
        <synopsis>ncwrite(filename, varname, vardata, [start, [stride]])</synopsis>
    </refsynopsisdiv>
    <refsection>
        <title>Paramètres</title>
        <variablelist>
            <varlistentry>
                <term>filename</term>
                <listitem><para>Chemin d'accès complet vers un fichier NetCDF (string).</para></listitem>
            </varlistentry>
            <varlistentry>
                <term>varname</term>
                <listitem><para>Nom d'une variable (string).</para></listitem>
            </varlistentry>
            <varlistentry>
                 <term>vardata</term>
                 <listitem><para>Données à écrire (scalaire, matrice, hypermatrice de double/entier, ou chaîne de caractères).</para></listitem>
            </varlistentry>
           <varlistentry>
                <term>start</term>
                <listitem><para>Optionnel, indices auxquels les données sont écrites dans chaque dimension (vecteur de double, les indices commencent à 1).</para></listitem>
            </varlistentry>
            <varlistentry>
                <term>stride</term>
                <listitem><para>Optionnel, espaces inter-élément dans chaque dimension (vecteur de double).</para></listitem>
            </varlistentry>
        </variablelist>
    </refsection>
    <refsection>
        <title>Description</title>
        <para><function>ncwrite</function> écrit les données <term>vardata</term> de la variable <term>varname</term> dans le fichier NetCDF <term>filename</term>.</para>

        <para>La variable et le fichier doivent être créés au préalable (grâce à la fonction <link linkend="nccreate">nccreate()</link> ou tout autre moyen).
        L'emplacement de la variable (i.e. le chemin du groupe dans lequel la variable est stockée) est spécifié dans <term>varname</term> en utilisant la forme <literal>/grp/subgrp/.../varname</literal>.</para>

        <para><term>vardata</term> peut être un scalaire, une matrice ou une hypermatrice du type de la variable (une conversion, si possible, aura lieu si le type diffère). Les variables avec une dimension illimitée seront étendues si nécessaire.</para>
        <para>Dans le cas de variable de type <literal>double/float</literal>, les valeurs manquantes ou égales à <literal>%nan</literal> sont automatiquement remplacées par la valeur de remplacement durant l'écriture. La valeur de remplacement est soit donnée en argument optionnel de la fonction <link linkend="nccreate">nccreate</link> ou par défaut la valeur définie par NetCDF.</para>

        <para>Les paramètres optionnels <term>start</term> and <term>stride</term> spécifient les indices auxquel les données sont écrites.
        Si des données existent déjà à ces indices, elles sont écrasées.</para>
    </refsection>
    <refsection>
        <title>Exemples</title>
        <programlisting role="example">
        <![CDATA[
        filename = fullfile(TMPDIR, 'vars.nc');

        // Ecrit le double 'x' 3.2
        nccreate(filename, 'x');
        ncwrite(filename, 'x', 3.2);

        // Ecrit la variable 'n' 100 (scalaire int8)
        nccreate(filename, 'n', 'Datatype', 'int8');
        ncwrite(filename, 'n', 100);

        // Ecrit la variable 'y' 1.0 dans le groupe 'grp'
        nccreate(filename, 'grp/y');
        ncwrite(filename, 'grp/y', 1.0);

        // Ecrit la chaîne de caractères  'Hello' dans la variable 'str'
        nccreate(filename, 'str', 'Datatype', 'char', 'Dimensions', list('len', 10));
        ncwrite(filename, 'str', 'Hello');

        // Ecrit les données du vecteur d'entiers 'vec'
        nccreate(filename, 'vec', 'Datatype', 'int32', 'Dimensions', list('dim', %inf));
        ncwrite(filename, 'vec',  [1 2 4 8 16]);

        // Ecrit les données de la matrice de double 'mat' de dimensions (3, 4)
        nccreate(filename, 'mat', 'Dimensions', list('r', 3, 'c', 4));
        ncwrite(filename, 'mat',  [1 2 3 4; 5 6 7 8; 9 10 11 12]);

        ncdisp(filename);
        ]]>
        </programlisting>
    </refsection>
    <refsection role="see also">
        <title>Voir aussi</title>
        <simplelist type="inline">
            <member><link linkend="nccreate">nccreate</link></member>
            <member><link linkend="ncread">ncread</link></member>
        </simplelist>
    </refsection>
</refentry>
