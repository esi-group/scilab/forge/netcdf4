README of sciNetCDF toolbox
---------------------------

This toolbox is a binding from the Unidata NetCDF4 library. It allows to read/write/manipulate NetCDF4 files under Scilab.
See http://www.unidata.ucar.edu/software/netcdf for more information.

This toolbox is currently in version 1.2.

Supported platforms:
- Windows 32/64 bits
- Linux 32/64 bits
- Scilab (version >= 5.5).

Installation:
- no pre-requisites needed.
- run from the toolbox directory:
    exec loader.sce

Compilation:
- prerequisites:
    - Windows: Visual Studio (2012 or 2013...) required.
    - Linux: standard compilers
- run from the toolbox directory:
    exec builder.sce

Tests:
- unit tests found in tests/unit_tests (for high level and low level functions)
- reading sample files found in tests/nonreg_tests
- to run all the tests, from the toolbox directory, use:
    test_run(pwd())

Known issues:
- on Linux, there might be some conflicts between Scilab and NetCDF about HDF5 libraries versions, which provokes crashes.
Set the environment variable HDF5_DISABLE_VERSION_CHECK to disable HDF5 version check, as following, before running Scilab:
    export HDF5_DISABLE_VERSION_CHECK=2
but test_run() however should fail as it launches Scilab as a separate process.
