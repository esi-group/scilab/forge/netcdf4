function array = mat2array(mat, typeName)
    if argn(2) <> 2 then
        error(msprintf(gettext("%s: Wrong number of input argument(s): %d expected.\n"), "mat2array", 2));
    end
    if ~ismatrix(mat) then
        error(msprintf(gettext("%s: Wrong type of input argument(s): a matrix expected.\n"), "mat2array", 1));
    end
    if typeof(typeName) <> "string"  then
        error(msprintf(gettext("%s: Wrong type for input argument #%d: A string expected.\n"), "mat2array", 2));
    end

    arrayTypeName = getArrayTypeName(typeName);

    if typeName == "char" then
      mat = strsplit(mat);
    end

    n = size(mat, '*');
    execstr(msprintf("array = new_%s(n);", arrayTypeName));

    for i = 0:n-1
      execstr(msprintf("%s_setitem(array, i, mat(i+1));", arrayTypeName));
    end
endfunction
