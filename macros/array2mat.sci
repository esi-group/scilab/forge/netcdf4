function mat = array2mat(array, arraySize, typeName)
    if argn(2) <> 3 then
        error(msprintf(gettext("%s: Wrong number of input argument(s): %d expected.\n"), "array2mat", 3));
    end
    if typeof(arraySize) <> "constant"  then
        error(msprintf(gettext("%s: Wrong type for input argument #%d: A double expected.\n"), "array2mat", 2));
    end
    if typeof(typeName) <> "string"  then
        error(msprintf(gettext("%s: Wrong type for input argument #%d: A string expected.\n"), "array2mat", 3));
    end

    arrayTypeName = getArrayTypeName(typeName);
    mat = [];
    for i=0:arraySize-1
        execstr(msprintf("mat($+1) = %s_getitem(array, i)", arrayTypeName));
    end

    if typeName == "char" then
        mat = strcat(mat);
    end
endfunction


