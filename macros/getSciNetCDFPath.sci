function scinetcdf_path = getSciNetCDFPath()
    [macros, toolbox_macros_path] = libraryinfo("scinetcdflib");
    scinetcdf_path = fullpath(toolbox_macros_path + '/..');
endfunction