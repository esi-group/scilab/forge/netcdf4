// Typemaps for output int arrays

%typemap(in, numinputs=0, noblock=1) (int *INOUT_SIZE, int* INOUT)
{
}

%typemap(arginit, noblock=1) (int *INOUT_SIZE, int* INOUT) ( int iarray_size, int iarray[32767] )
{
  $1 = &iarray_size;
  $2 = &iarray[0];
}

%typemap(argout, noblock=1, fragment="SWIG_SciDouble_FromIntArrayAndSize") (int *INOUT_SIZE, int* INOUT)
{
  if (SWIG_SciDouble_FromIntArrayAndSize(pvApiCtx, SWIG_Scilab_GetOutputPosition(), 1, *$1, $2) == SWIG_OK) {
    SWIG_Scilab_SetOutput(pvApiCtx, SWIG_NbInputArgument(pvApiCtx) + SWIG_Scilab_GetOutputPosition());
  }
  else {
    return SWIG_ERROR;
  }
}

%typemap(freearg, noblock=1) (int *INOUT_SIZE, int* INOUT)
{
}

