// typemap for name input 

%typemap(in, noblock=1, fragment="SWIG_AsCharPtrAndSize") const char *name
{
  if (SWIG_AsCharPtrAndSize($input, &$1, NULL, 0) != SWIG_OK) {
    return SWIG_ERROR;
  }
}

// typemap for name output

%typemap(in, numinputs=0) char *out_name
{
}

%typemap(arginit, noblock=1) char *out_name
{
  $1 = (char*) calloc(NC_MAX_NAME, sizeof(char));
}

%typemap(argout, noblock=1, fragment="SWIG_FromCharPtr") char *out_name
{
  %set_output(SWIG_FromCharPtr($1));
}

%typemap(freearg, noblock=1) char *out_name
{
  free($1);
}

// typemaps for names output with length

// (size_t *name_len, char *name)

%typemap(in, numinputs=0, noblock=1) (size_t *name_len, char *name)
{
}

%typemap(arginit, noblock=1) (size_t *name_len, char *name) (size_t tmp_len)
{
  $1 = &tmp_len;
  $2 = (char*) calloc(NC_MAX_NAME, sizeof(char));
}

%typemap(argout, noblock=1, fragment="SWIG_FromCharPtr") (size_t *name_len, char *name)
{
  %set_output(SWIG_FromCharPtr($2));
}

%typemap(freearg, noblock=1) (size_t *name_len, char *name)
{
  free($2);
}



