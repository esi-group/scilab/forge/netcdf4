%module libscinetcdf

%include <typemaps.i>

%{
#include "netcdf.h"
%}

#undef EXTERNL

%scilabconst(1);

%include scinetcdf_low_level.i
%include scinetcdf_high_level.i

%ignore ncerr;
%ignore ncopts;
%ignore nccreate;
%ignore ncread;
%ignore ncwrite;

%include "netcdf.h"
