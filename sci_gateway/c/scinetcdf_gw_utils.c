#include <stdlib.h>
#include <string.h>

size_t getHypermatSize(size_t *pszDims, int iNbDims)
{
  if (iNbDims > 0) {
    size_t size = 1;
    int i;
    for (i=0; i<iNbDims; i++) {
      size *= pszDims[i];
    }
    return size;
  }
  else return 0;
}

void sciToNcDims(int *piDims, int iNbDims, size_t *pszDims)
{
  if (iNbDims > 0) {
    if (iNbDims > 1) {
      int i;
      pszDims[iNbDims-2] = piDims[0];
      pszDims[iNbDims-1] = piDims[1];
      for (i=0; i<iNbDims-2; i++) {
          pszDims[i] = piDims[iNbDims-i-1];
      }
    }
    else {
      pszDims[0] = piDims[0];
    }
  }
}

void ncToSciDims(size_t *pszDims, int iNbDims, int **piDims)
{
  if (iNbDims > 0) {
    if (iNbDims > 1) {
      int i;
      *piDims = (int*) malloc(iNbDims * sizeof(int));
      (*piDims)[0] = pszDims[iNbDims-2];
      (*piDims)[1] = pszDims[iNbDims-1];
      for (i=2; i<iNbDims; i++) {
          (*piDims)[i] = pszDims[iNbDims-i-1];
      }
    }
    else {
      *piDims = (int*) malloc(2 * sizeof(int));
      (*piDims)[0] = 1;
      (*piDims)[1] = pszDims[0];
    }
  }
}




void transposeMat(int iDim1, int iDim2, char *pcSrc, char *pcDst, int iTypeSize)
{
  int i,j;
  for (i=0; i<iDim1; i++) {
    for (j=0; j<iDim2; j++) {
      memcpy(&pcDst[(j*iDim1+i)*iTypeSize], &pcSrc[(i*iDim2+j)*iTypeSize], iTypeSize);
    }
  }
}

void transposeMatsInHypermat(int iDim1, int iDim2,
  char *pcSrc, char *pcDst, int sizeHypermat, int iTypeSize)
{
  int sizeMat;
  if (sizeHypermat < 1) {
    return;
  }
  sizeMat = iDim1 * iDim2;
  if ((iDim1 > 1) && (iDim2 > 1)) {
    int step = sizeMat * iTypeSize;
    int i;
    for (i=0; i < sizeHypermat/sizeMat; i++) {
      transposeMat(iDim1, iDim2, pcSrc, pcDst, iTypeSize);
      pcSrc += step;
      pcDst += step;
    }
  }
  else {
    memcpy(pcDst, pcSrc, sizeMat*iTypeSize);
  }
}

void ncToSciData(size_t *pszDims, int iNbDims, int **piDims,
  void *pvSrc, void *pvDst, int iTypeSize)
{
  size_t size = getHypermatSize(pszDims, iNbDims);
  ncToSciDims(pszDims, iNbDims, piDims);

  if (iNbDims >= 2) {
    transposeMatsInHypermat((*piDims)[0], (*piDims)[1],
      (char*)pvSrc, (char*)pvDst, size, iTypeSize);
  }
  else if (iNbDims == 1) {
    memcpy(pvDst, pvSrc, size*iTypeSize);
  }
}

void sciToNcData(int *piDims, int iNbDims,
  size_t **pszDims, void *pvSrc, void **pvDst, int iTypeSize)
{
  size_t size;
  *pszDims = (size_t*) malloc(iNbDims * sizeof(size_t));
  sciToNcDims(piDims, iNbDims, *pszDims);

  size = getHypermatSize(*pszDims, iNbDims);
  *pvDst = (void*) malloc(size * iTypeSize);

  if (iNbDims >= 2) {
    transposeMatsInHypermat(piDims[1], piDims[0],
      (char*)pvSrc, (char*)*pvDst, size, iTypeSize);
  }
  else if (iNbDims == 1) {
    memcpy(*pvDst, pvSrc, size*iTypeSize);
  }
}
