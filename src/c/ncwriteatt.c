#include "scinetcdf_hl.h"
#include "scinetcdf_hl_utils.h"

#include "netcdf.h"

#include <stdlib.h>
#include <string.h>

SCINETCDF_HL_API void imp_ncwriteatt(char *source, char *location,
  char *attname, st_attdata attdata, char **errmsg)
{
    int ret;
    int ncid;
    int grpid;
    int varid;
    char *varname = NULL;

    if (attdata.len < 1)
    {
        return;
    }

    if ((ret = nc_create(source, NC_NETCDF4 | NC_NOCLOBBER, &ncid)))
    {
        if (ret == NC_EEXIST)
        {
            if ((ret = nc_open(source, NC_WRITE, &ncid)))
            {
                *errmsg = getNetcdfErrorMsg("nc_open()", ret);
                return;
            }
        }
        else
        {
            *errmsg = getNetcdfErrorMsg("nc_create()", ret);
            return;
        }
    }

    if ((ret = createGroup(ncid, location, &grpid, &varname, errmsg)))
    {
        nc_close(ncid);
        return;
    }

    if ((ret = getVarId(grpid, varname, &varid, errmsg)))
    {
        nc_close(ncid);
        return;
    }

    if ((ret = nc_put_att(grpid, varid, attname, attdata.type, attdata.len,
        attdata.data)))
    {
        *errmsg = getNetcdfErrorMsg("nc_put_att()", ret);
        nc_close(ncid);
        return;
    }

    if ((ret = nc_close(ncid)))
    {
        *errmsg = getNetcdfErrorMsg("nc_close()", ret);
        return;
    }
}

