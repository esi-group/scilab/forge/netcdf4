#include "scinetcdf_hl_utils.h"

#include "netcdf.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

char *getErrorMsg(const char *funcName, int err, const char *errDesc)
{
    char *errMsg = (char *) malloc(256 * sizeof(char));
    if (errDesc)
      sprintf(errMsg, "%s returned the error %d: %s.", funcName, err,
        errDesc);
    else
      sprintf(errMsg, "%s returned the error %d.", funcName, err);
    return errMsg;
}

char *getNetcdfErrorMsg(const char *funcName, int err)
{
    return getErrorMsg(funcName, err, nc_strerror(err));
}

static void parseFullVarName(char *fullvarname, char ***grpnames,
  int *nbgroups, char **grppath, char **varname)
{
    char *pch;
    char *pchNext;
    char *pchEnd;
    int parent_grpid;
    int i;

    *grpnames = NULL;
    i = 0;
    pch = fullvarname;
    pchNext = strchr(fullvarname, '/');
    while (pchNext != NULL)
    {
        if (pchNext-pch > 0)
        {
            if (*grpnames == NULL)
                *grpnames = (char **) malloc(128 * sizeof(char *));
            (*grpnames)[i] = (char*) calloc(pchNext-pch+1, sizeof(char));
            strncpy((*grpnames)[i], pch, pchNext-pch);
            i++;
        }
        pch = pchNext+1;
        pchNext = strchr(pch, '/');
    }
    *nbgroups = i;

    if (*nbgroups > 0)
    {
      *grppath = (char*) calloc(pch-fullvarname, sizeof(char));
      strncpy(*grppath, fullvarname, pch-fullvarname-1);
    }

    pchEnd = fullvarname+strlen(fullvarname);
    *varname = (char*) calloc(pchEnd-pch+1, sizeof(char));
    strncpy(*varname, pch, pchEnd-pch);
}

int getVarId(int ncid, char *varname, int *varid, char **errmsg)
{
    if ((varname != NULL) && (strlen(varname) > 0))
    {
        int ret;
        if ((ret = nc_inq_varid(ncid, varname, varid)))
        {
            *errmsg = getNetcdfErrorMsg("nc_inq_varid()", ret);
            return ret;
        }
    }
    else
    {
        *varid = NC_GLOBAL;
        return 0;
    }
}

int getGroupAndVarIds(int ncid, char *fullvarname, int *grpid,
  int *varid, char **errmsg)
{
    int ret;
    char *grppath = NULL;
    char **grpnames = NULL;
    int nbgroups;
    char *varname = NULL;
    int i;

    parseFullVarName(fullvarname, &grpnames, &nbgroups, &grppath, &varname);

    for (i=0; i<nbgroups; i++)
    {
        free(grpnames[i]);
    }
    free(grpnames);

    if (grppath != NULL)
    {
        if ((ret = nc_inq_grp_full_ncid(ncid, grppath, grpid)))
        {
            *errmsg = getNetcdfErrorMsg("nc_inq_grp_full_ncid()", ret);
            free(grppath);
            return ret;
        }
    }
    else
    {
        *grpid = ncid;
    }

    free(grppath);

    if ((ret = getVarId(*grpid, varname, varid, errmsg)))
    {
        return ret;
    }

    return 0;
}

int createGroup(int ncid, char *fullvarname, int *grpid, char **varname,
  char **errmsg)
{
    char *grppath = NULL;
    char **grpnames = NULL;
    int nbgroups;
    int parent_grpid;
    int ret;
    int i;

    *grpid = ncid;
    parent_grpid = ncid;

    parseFullVarName(fullvarname, &grpnames, &nbgroups, &grppath, varname);

    for (i=0; i<nbgroups; i++)
    {
        if ((ret = nc_inq_grp_ncid(parent_grpid, grpnames[i], grpid)))
        {
            if (ret == NC_ENOGRP)
            {
                if ((ret = nc_def_grp(parent_grpid, grpnames[i], grpid)))
                {
                    *errmsg = getNetcdfErrorMsg("nc_def_grp()", ret);
                    return ret;
                }
            }
            else
            {
                *errmsg = getNetcdfErrorMsg("nc_nc_inq_grp_ncid()", ret);
                return ret;
            }
        }
        parent_grpid = *grpid;
    }

    for (i=0; i<nbgroups; i++)
    {
        free(grpnames[i]);
    }
    free(grpnames);
    free(grppath);

    return 0;
}

int getFillValue(int ncid, int varid, int *fillMode, double *dFillValue, char **errmsg)
{
    int ret;

    if ((ret = nc_inq_var_fill(ncid, varid, fillMode, NULL)))
    {
        *errmsg = getNetcdfErrorMsg("nc_inq_var_fill", ret);
        return ret;
    }

    if (*fillMode == NC_FILL)
    {
        int vartype;

        if ((ret = nc_inq_vartype(ncid, varid, &vartype)))
        {
            *errmsg = getNetcdfErrorMsg("nc_inq_var_vartype", ret);
            return ret;
        }

        switch(vartype)
        {
            case NC_BYTE :
            {
                signed char scFillValue;
                if ((ret = nc_inq_var_fill(ncid, varid, NULL, &scFillValue)))
                {
                    *errmsg = getNetcdfErrorMsg("nc_inq_var_fill", ret);
                    return ret;
                }
                *dFillValue = (double) scFillValue;
                break;
            }
            case NC_UBYTE :
            {
                unsigned char ucFillValue;
                if ((ret = nc_inq_var_fill(ncid, varid, NULL, &ucFillValue)))
                {
                    *errmsg = getNetcdfErrorMsg("nc_inq_var_fill", ret);
                    return ret;
                }
                *dFillValue = (double) ucFillValue;
                break;
            }
            case NC_SHORT :
            {
                short sFillValue;
                if ((ret = nc_inq_var_fill(ncid, varid, NULL, &sFillValue)))
                {
                    *errmsg = getNetcdfErrorMsg("nc_inq_var_fill", ret);
                    return ret;
                }
                *dFillValue = (double) sFillValue;
                break;
            }
            case NC_USHORT :
            {
                unsigned short usFillValue;
                if ((ret = nc_inq_var_fill(ncid, varid, NULL, &usFillValue)))
                {
                    *errmsg = getNetcdfErrorMsg("nc_inq_var_fill", ret);
                    return ret;
                }
                *dFillValue = (double) usFillValue;
                break;
            }
            case NC_INT :
            {
                int iFillValue;
                if ((ret = nc_inq_var_fill(ncid, varid, NULL, &iFillValue)))
                {
                    *errmsg = getNetcdfErrorMsg("nc_inq_var_fill", ret);
                    return ret;
                }
                *dFillValue = (double) iFillValue;
                break;
            }
            case NC_UINT :
            {
                unsigned int uiFillValue;
                if ((ret = nc_inq_var_fill(ncid, varid, NULL, &uiFillValue)))
                {
                    *errmsg = getNetcdfErrorMsg("nc_inq_var_fill", ret);
                    return ret;
                }
                *dFillValue = (double) uiFillValue;
                break;
            }
            case NC_FLOAT :
            {
                float fFillValue;
                if ((ret = nc_inq_var_fill(ncid, varid, NULL, &fFillValue)))
                {
                    *errmsg = getNetcdfErrorMsg("nc_inq_var_fill", ret);
                    return ret;
                }
                *dFillValue = (double) fFillValue;
                break;
            }
            case NC_DOUBLE :
            {
                if ((ret = nc_inq_var_fill(ncid, varid, NULL, &*dFillValue)))
                {
                    *errmsg = getNetcdfErrorMsg("nc_inq_var_fill", ret);
                    return ret;
                }
                break;
            }
        }
    }
    return 0;
}
