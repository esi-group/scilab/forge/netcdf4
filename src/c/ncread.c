#include "scinetcdf_hl.h"
#include "scinetcdf_hl_utils.h"

#include "netcdf.h"

#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef _MSC_VER
    #ifndef NAN
        static const unsigned long __nan[2] = {0xffffffff, 0x7fffffff};
        #define NAN (*(const float *) __nan)
    #endif
#endif

SCINETCDF_HL_API void imp_ncread(char *source, char *fullvarname, st_data *data, char **errmsg, size_t *start, size_t *count, ptrdiff_t *stride)
{
    int ret;
    int ncid;
    char *varname = NULL;
    int grpid;
    int varid;
    int dimids[NC_MAX_VAR_DIMS];
    int nbattsp;
    char dummy[NC_MAX_NAME];
    size_t size;
    int fillMode;
    double dFillValue;
    int i;

    data->nbdims = 0;
    data->type = 0;
    data->dims = NULL;
    data->data = NULL;

    if ((ret = nc_open(source, NC_NOWRITE, &ncid)))
    {
        *errmsg = getNetcdfErrorMsg("nc_open()", ret);
        return;
    }

    if ((ret = getGroupAndVarIds(ncid, fullvarname, &grpid, &varid, errmsg)))
    {
        nc_close(ncid);
        return;
    }

    if ((ret = nc_inq_var(grpid, varid, dummy, &(data->type), &(data->nbdims), &dimids[0], &nbattsp)))
    {
        *errmsg = getNetcdfErrorMsg("nc_inq_var()", ret);
        nc_close(ncid);
        return;
    }

    size = 1;
    if (data->nbdims > 0)
    {
        int i;
        if (start)
        {
            for (i=0; i<data->nbdims; i++)
            {
                start[i]--;
            }
        }
        else
        {
            start = (size_t *) malloc(data->nbdims * sizeof(size_t));
            memset(start, 0, data->nbdims * sizeof(size_t));
        }

        if (!count)
        {
            count = (size_t *) malloc(data->nbdims * sizeof(size_t));
            for (i = 0; i < data->nbdims; i++)
            {
                size_t len;
                if ((ret = nc_inq_dimlen(grpid, dimids[i], &len)))
                {
                    *errmsg = getNetcdfErrorMsg("nc_inq_dimlen()", ret);
                    nc_close(ncid);
                    return;
                }
                count[i] = len - start[i];
                if (stride != NULL)
                {
                    count[i] = count[i] / stride[i];
                }
            }
        }

        for (i = 0; i < data->nbdims; i++)
        {
            size *= count[i];
        }

        data->dims = (size_t *) malloc(data->nbdims * sizeof(size_t));
        for (i = 0; i < data->nbdims; i++)
        {
            data->dims[i] = count[i];
        }
    }
    else
    {
        start = (size_t *) malloc(sizeof(size_t));
        *start = 0;
        count = (size_t *) malloc(sizeof(size_t));
        *count = 1;
        stride = NULL;

        data->dims = NULL;
    }

    if (size > 0)
    {
        switch(data->type)
        {
            case NC_CHAR:
            {
                size++;
                data->data = malloc(size * sizeof(signed char));
                memset(data->data, 0, size);
                ret = nc_get_vars_text(grpid, varid, start, count, stride, (char*)data->data);
                break;
            }
            case NC_BYTE:
            {
                data->data = malloc(size * sizeof(signed char));
                ret = nc_get_vars_schar(grpid, varid, start, count, stride, (signed char*)data->data);
                break;
            }
            case NC_UBYTE:
            {
                data->data = malloc(size * sizeof(unsigned char));
                ret = nc_get_vars_uchar(grpid, varid, start, count, stride, (unsigned char*)data->data);
                break;
            }
            case NC_SHORT:
            {
                data->data = malloc(size * sizeof(short));
                ret = nc_get_vars_short(grpid, varid, start, count, stride, (short*)data->data);
                break;
            }
            case NC_USHORT:
            {
                data->data = malloc(size * sizeof(unsigned short));
                ret = nc_get_vars_ushort(grpid, varid, start, count, stride, (unsigned short*)data->data);
                break;
            }
            case NC_INT:
            {
                data->data = malloc(size * sizeof(int));
                ret = nc_get_vars_int(grpid, varid, start, count, stride, (int*)data->data);
                break;
            }
            case NC_UINT:
            {
                data->data = malloc(size * sizeof(unsigned int));
                ret = nc_get_vars_uint(grpid, varid, start, count, stride, (unsigned int*)data->data);
                break;
            }
            case NC_FLOAT:
            case NC_DOUBLE:
            {
                data->data = malloc(size * sizeof(double));
                ret = nc_get_vars_double(grpid, varid, start, count, stride, (double*)data->data);
                break;
            }
        }

        if (ret != NC_NOERR)
        {
            *errmsg = getNetcdfErrorMsg("nc_get_var()", ret);
            nc_close(ncid);
            return;
        }

        if ((data->type == NC_DOUBLE) || (data->type == NC_FLOAT))
        {
            if ((ret = getFillValue(grpid, varid, &fillMode, &dFillValue, errmsg)))
            {
                nc_close(ncid);
                return;
            }

            if (fillMode == NC_FILL)
            {
                for (i=0; i<size; i++)
                {
                    double *dValue = &(((double*)data->data)[i]);
                    if (fabs(*dValue-dFillValue) < 1E-15)
                    {
                        *dValue = NAN;
                    }
                }
            }
        }
    }

    if ((ret = nc_close(ncid)))
    {
        *errmsg = getNetcdfErrorMsg("nc_close()", ret);
        return;
    }
}

