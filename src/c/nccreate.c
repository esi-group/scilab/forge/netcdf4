#include "scinetcdf_hl.h"
#include "scinetcdf_hl_utils.h"

#include "netcdf.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <math.h>

#ifdef _MSC_VER
#  include <float.h>
#  define ISINF(x) !_finite(x)
#else
#  include <math.h>
#  define ISINF(x) isinf(x)
#endif

static void sciToNcDimensionList(st_option_list *child_option_list, int *ndims, char ***dimnames, size_t **dimsizes);
static void sciToNcDimension(st_option *child_option, char **dimname, size_t *dimsize);

SCINETCDF_HL_API void imp_nccreate(char *filename, char *fullvarname, char **errmsg, ...)
{
    int ret;
    int ncid;
    char *varname = NULL;
    int grpid;
    va_list opt_args;
    st_option_list *option_list;
    int ndims;
    size_t *dimsizes;
    char **dimnames;
    int dimids[NC_MAX_VAR_DIMS];
    nc_type datatype;
    int varid;
    int fillMode;
    void *fillValue;
    int i;

    if ((ret = nc_create(filename, NC_NETCDF4 | NC_NOCLOBBER, &ncid)))
    {
        if (ret == NC_EEXIST)
        {
            if ((ret = nc_open(filename, NC_WRITE, &ncid)))
            {
                *errmsg = getNetcdfErrorMsg("nc_open()", ret);
                return;
            }
        }
        else
        {
            *errmsg = getNetcdfErrorMsg("nc_create()", ret);
            return;
        }
    }

    if ((ret = createGroup(ncid, fullvarname, &grpid, &varname, errmsg)))
    {
        nc_close(ncid);
        return;
    }

    ndims = 0;
    datatype = NC_DOUBLE;
    fillMode = -1;
    fillValue = NULL;

    va_start(opt_args, errmsg);
    option_list = va_arg(opt_args, st_option_list*);
    for (i=0; i<option_list->nboptions; i++)
    {
        st_option *option = &(option_list->options[i]);

        char *name = option->name;
        if (strcmp(name, "Dimensions") == 0)
        {
            st_option_list *child_option_list = (st_option_list*) option->value;
            sciToNcDimensionList(child_option_list, &ndims, &dimnames, &dimsizes);
        }
        else if (strcmp(name, "Datatype") == 0)
        {
            char *pstDatatype = (char *) option->value;

            if (strcmp(pstDatatype, "double") == 0)
                datatype = NC_DOUBLE;
            else if (strcmp(pstDatatype, "float") == 0)
                datatype = NC_FLOAT;
            else if (strcmp(pstDatatype, "int32") == 0)
                datatype = NC_INT;
            else if (strcmp(pstDatatype, "uint32") == 0)
                datatype = NC_UINT;
            else if (strcmp(pstDatatype, "int16") == 0)
                datatype = NC_SHORT;
            else if (strcmp(pstDatatype, "uint16") == 0)
                datatype = NC_USHORT;
            else if (strcmp(pstDatatype, "int8") == 0)
                datatype = NC_BYTE;
            else if (strcmp(pstDatatype, "uint8") == 0)
                datatype = NC_UBYTE;
            else if (strcmp(pstDatatype, "char") == 0)
                datatype = NC_CHAR;
            else if ((strcmp(pstDatatype, "uint64") == 0) || (strcmp(pstDatatype, "int64") == 0))
            {
                *errmsg = (char *) malloc(256 * sizeof(char));
                sprintf(*errmsg, "64 bit types are not supported in Scilab 5.");
                nc_close(ncid);
                return;
            }
            else
            {
                *errmsg = (char *) malloc(256 * sizeof(char));
                sprintf(*errmsg, "Unknown type '%s', "
              "supported types are 'double|float|int32|uint32|int16|uint16|int8|uint8|char'.", pstDatatype);
                nc_close(ncid);
                return;
            }
        }
        else if (strcmp(name, "FillValue") == 0)
        {
            if (option->type == OT_STRING)
            {
                char *pstValue = (char *) option->value;
                if (strcmp(pstValue, "disable") == 0)
                {
                    fillMode = NC_NOFILL;
                }
                else if (strlen(pstValue) == 1)
                {
                    fillMode = NC_FILL;
                    fillValue = option->value;
                }
            }
            else
            {
                fillMode = NC_FILL;
                fillValue = option->value;
            }
        }
        else
        {
            *errmsg = (char *) malloc(256 * sizeof(char));
            sprintf(*errmsg, "Unknown optional argument '%s'.", name);
            nc_close(ncid);
            return;
        }
    }
    va_end(opt_args);

    for (i = 0; i < ndims; i++)
    {
        if ((ret = nc_inq_dimid(grpid, dimnames[i], &dimids[i])) == NC_EBADDIM)
        {
            if ((ret = nc_def_dim(grpid, dimnames[i], dimsizes[i], &dimids[i])))
            {
                *errmsg = getNetcdfErrorMsg("nc_def_dim()", ret);
                nc_close(ncid);
                return;
            }
        }
        else
        {
            size_t dimsize;
            if ((ret = nc_inq_dimlen(grpid, dimids[i], &dimsize)))
            {
                *errmsg = getNetcdfErrorMsg("nc_inq_dimlen()", ret);
                nc_close(ncid);
                return;
            }
            if (dimsize != dimsizes[i])
            {
                *errmsg = (char *) malloc(256 * sizeof(char));
                sprintf(*errmsg, "The dimension '%s' already exists, but with a different size.", dimnames[i]);
                nc_close(ncid);
                return;
            }
        }
    }

    if ((ret = nc_def_var(grpid, varname, datatype, ndims, dimids, &varid)))
    {
        *errmsg = getNetcdfErrorMsg("nc_def_var()", ret);
        nc_close(ncid);
        return;
    }

    if (fillMode != -1)
    {
        if ((ret = nc_def_var_fill(grpid, varid, fillMode, fillValue)))
        {
            *errmsg = getNetcdfErrorMsg("nc_def_var_fill()", ret);
            nc_close(ncid);
            return;
        }
    }

    if ((ret = nc_close(ncid)))
    {
        *errmsg = getNetcdfErrorMsg("nc_close()", ret);
        return;
    }
}

void sciToNcDimensionList(st_option_list *child_option_list, int *ndims, char ***dimnames, size_t **dimsizes)
{
    *ndims = child_option_list->nboptions;
    if (*ndims > 0) {
        *dimnames = (char**) malloc(*ndims * sizeof(char*));
        *dimsizes = (size_t*) malloc(*ndims * sizeof(size_t));
        if (*ndims > 1) {
            int i;
            sciToNcDimension(&(child_option_list->options[0]), &((*dimnames)[*ndims-2]), &((*dimsizes)[*ndims-2]));
            sciToNcDimension(&(child_option_list->options[1]), &((*dimnames)[*ndims-1]), &((*dimsizes)[*ndims-1]));

            for (i=0; i<*ndims-2; i++) {
                sciToNcDimension(&(child_option_list->options[*ndims-i-1]), &((*dimnames)[i]), &((*dimsizes)[i]));
            }
        }
        else {
            sciToNcDimension(&(child_option_list->options[0]), &((*dimnames)[0]), &((*dimsizes)[0]));
        }
    }
}

void sciToNcDimension(st_option *child_option, char **dimname, size_t *dimsize)
{
    double dvalue;
    *dimname = child_option->name;
    dvalue = *((double*)child_option->value);
    *dimsize = (size_t)ISINF(dvalue) == 0?dvalue:NC_UNLIMITED;
}
