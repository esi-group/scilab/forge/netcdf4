[bOK, ilib] = c_link('imp_nccreate');
if bOK then
  ulink(ilib);
end
//
[bOK, ilib] = c_link('imp_ncread');
if bOK then
  ulink(ilib);
end
//
[bOK, ilib] = c_link('imp_ncwrite');
if bOK then
  ulink(ilib);
end
//
[bOK, ilib] = c_link('imp_ncwriteatt');
if bOK then
  ulink(ilib);
end
//
[bOK, ilib] = c_link('imp_ncreadatt');
if bOK then
  ulink(ilib);
end

clear bOK;
clear ilib;
