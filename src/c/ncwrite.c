#include "scinetcdf_hl.h"
#include "scinetcdf_hl_utils.h"

#include "netcdf.h"

#include <stdlib.h>
#include <string.h>

#ifdef _MSC_VER
#  include <float.h>
#  define ISNAN(x) _isnan(x)
#else
#  include <math.h>
#  define ISNAN(x) isnan(x)
#endif

SCINETCDF_HL_API void imp_ncwrite(char *filename, char *fullvarname, st_data data, char **errmsg, size_t *start, ptrdiff_t *stride)
{
    int ret;
    int ncid;
    int grpid;
    char *varname = NULL;
    int varid;
    size_t *count = NULL;
    size_t size = 0;
    int ndims = 0;

    if ((ret = nc_open(filename, NC_NETCDF4 + NC_WRITE, &ncid)))
    {
        *errmsg = getNetcdfErrorMsg("nc_open()", ret);
        return;
    }

    if ((ret = getGroupAndVarIds(ncid, fullvarname, &grpid, &varid, errmsg)))
    {
        nc_close(ncid);
        return;
    }

    if ((ret = nc_inq_varndims(grpid, varid, &ndims)))
    {
        *errmsg = getNetcdfErrorMsg("nc_inq_varndims()", ret);
        nc_close(ncid);
        return;
    }

    if (ndims > 0)
    {
        // variable is an array (it has dimensions)
        int i;
        int nmissingdims;

        // are start indexs specified (indexs at which data are wrote in each dimension) ?
        if (start)
        {
            // decrement them, since indexs begin with 0, not 1
            for (i=0; i<ndims; i++)
            {
                start[i]--;
            }
        }
        else
        {
            // initialize all indexs at 0
            start = (size_t *) malloc(ndims * sizeof(size_t));
            memset(start, 0, ndims * sizeof(size_t));
        }

        // set counters (size of data in each dimension to write)
        count = (size_t *) malloc(ndims * sizeof(size_t));

        if (ndims >= data.nbdims)
        {
            // missing dimensions in data to write, initialize them to 1
            nmissingdims = ndims-data.nbdims;
            for (i=0; i<nmissingdims; i++)
            {
                count[i] = 1;
            }
            size = 1;
            for (i=nmissingdims; i<ndims; i++)
            {
                count[i] = data.dims[i-nmissingdims];
                size *= count[i];
            }
        }
        else
        {
            // more dimensions in data to write
            if (ndims > 1)
            {
                // ignore extra dimensions
                size = 1;
                for (i=0; i<ndims; i++)
                {
                    count[i] = data.dims[i];
                    size *= count[i];
                }
            }
            else
            {
                // special case for vector, take all data
                size = 1;
                for (i=0; i<data.nbdims; i++)
                {
                    size *= data.dims[i];
                }
                count[0] = size;
            }
        }
    }
    else
    {
        // variable is a scalar
        size = 1;
        start = (size_t *) malloc(sizeof(size_t));
        *start = 0;
        count = (size_t *) malloc(sizeof(size_t));
        *count = 1;
        stride = NULL;
    }

    if (size > 0)
    {
        switch (data.type)
        {
            case NC_CHAR:
            {
                if ((ret = nc_put_vars_text(grpid, varid, start, count,
                  stride, (const char *)data.data)))
                {
                    *errmsg = getNetcdfErrorMsg("nc_put_vars_text", ret);
                    nc_close(ncid);
                    return;
                }
                break;
            }
            case NC_BYTE:
            {
                if ((ret = nc_put_vars_schar(grpid, varid, start, count,
                  stride, (const signed char *)data.data)))
                {
                    *errmsg = getNetcdfErrorMsg("nc_put_vars_schar()", ret);
                    nc_close(ncid);
                    return;
                }
                break;
            }
            case NC_UBYTE:
            {
                if ((ret = nc_put_vars_uchar(grpid, varid, start, count,
                  stride, (const unsigned char *)data.data)))
                {
                    *errmsg = getNetcdfErrorMsg("nc_put_vars_uchar()", ret);
                    nc_close(ncid);
                    return;
                }
                break;
            }
            case NC_SHORT:
            {
                if ((ret = nc_put_vars_short(grpid, varid, start, count,
                  stride, (const short *)data.data)))
                {
                    *errmsg = getNetcdfErrorMsg("nc_put_vars_short()", ret);
                    nc_close(ncid);
                    return;
                }
                break;
            }
            case NC_USHORT:
            {
                if ((ret = nc_put_vars_ushort(grpid, varid, start, count,
                  stride, (const unsigned short *)data.data)))
                {
                    *errmsg = getNetcdfErrorMsg("nc_put_vars_ushort()", ret);
                    nc_close(ncid);
                    return;
                }
                break;
            }
            case NC_INT:
            {
                if ((ret = nc_put_vars_int(grpid, varid, start, count,
                  stride, (const int *)data.data)))
                {
                    *errmsg = getNetcdfErrorMsg("nc_put_vars_int()", ret);
                    nc_close(ncid);
                    return;
                }
                break;
            }
            case NC_UINT:
            {
                if ((ret = nc_put_vars_uint(grpid, varid, start, count,
                  stride, (const unsigned int *)data.data)))
                {
                    *errmsg = getNetcdfErrorMsg("nc_put_vars_uint()", ret);
                    nc_close(ncid);
                    return;
                }
                break;
            }
            case NC_DOUBLE:
            {
                int fillMode;
                double dFillValue;

                if ((ret = getFillValue(grpid, varid, &fillMode, &dFillValue, errmsg)))
                {
                    nc_close(ncid);
                    return;
                }

                if (fillMode == NC_FILL)
                {
                    int i;
                    for (i=0; i<size; i++)
                    {
                        double *dValue = &(((double*)data.data)[i]);
                        if (ISNAN(*dValue))
                        {
                            *dValue = dFillValue;
                        }
                    }
                }

                if ((ret = nc_put_vars_double(grpid, varid, start, count,
                  stride, (const double *)data.data)))
                {
                    *errmsg = getNetcdfErrorMsg("nc_put_vars_double()", ret);
                    nc_close(ncid);
                    return;
                }
                break;
            }
        }
    }

    free(start);
    free(stride);
    free(count);

    if ((ret = nc_close(ncid)))
    {
        *errmsg = getNetcdfErrorMsg("nc_close()", ret);
        return;
    }
}

