#include "scinetcdf_hl.h"
#include "scinetcdf_hl_utils.h"

#include "netcdf.h"

#include <stdlib.h>
#include <string.h>

SCINETCDF_HL_API void imp_ncreadatt(char *source, char *location,
  char *attname, st_attdata *attdata, char **errmsg)
{
    int ret;
    int ncid;
    int grpid;
    int varid;

    if ((ret = nc_open(source, NC_NOWRITE, &ncid)))
    {
        *errmsg = getNetcdfErrorMsg("nc_open()", ret);
        return;
    }

    if ((ret = getGroupAndVarIds(ncid, location, &grpid, &varid, errmsg)))
    {
        nc_close(ncid);
        return;
    }

    if ((ret = nc_inq_att(grpid, varid, attname, &attdata->type, &attdata->len)))
    {
        *errmsg = getNetcdfErrorMsg("nc_inq_att()", ret);
        nc_close(ncid);
        return;
    }

    switch(attdata->type)
    {
        case NC_CHAR:
        {
            attdata->len++;
            attdata->data = malloc(attdata->len * sizeof(signed char));
            memset(attdata->data, 0, attdata->len);
            break;
        }
        case NC_BYTE:
        {
            attdata->data = malloc(attdata->len * sizeof(signed char));
            break;
        }
        case NC_UBYTE:
        {
            attdata->data = malloc(attdata->len * sizeof(unsigned char));
            break;
        }
        case NC_SHORT:
        {
            attdata->data = malloc(attdata->len * sizeof(short));
            break;
        }
        case NC_USHORT:
        {
            attdata->data = malloc(attdata->len * sizeof(unsigned short));
            break;
        }
        case NC_INT:
        {
            attdata->data = malloc(attdata->len * sizeof(int));
            break;
        }
        case NC_UINT:
        {
            attdata->data = malloc(attdata->len * sizeof(unsigned int));
            break;
        }
        case NC_FLOAT:
        {
            attdata->data = malloc(attdata->len * sizeof(float));
            break;
        }
        case NC_DOUBLE:
        {
            attdata->data = malloc(attdata->len * sizeof(double));
            break;
        }
    }

    if ((ret = nc_get_att(grpid, varid, attname, attdata->data)))
    {
        *errmsg = getNetcdfErrorMsg("nc_get_att()", ret);
        nc_close(ncid);
        return;
    }

    if ((ret = nc_close(ncid)))
    {
        *errmsg = getNetcdfErrorMsg("nc_close()", ret);
        return;
    }
}

