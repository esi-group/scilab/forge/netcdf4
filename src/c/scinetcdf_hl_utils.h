#ifndef SCINETCDF_HL_UTILS
#define SCINETCDF_HL_UTILS

char *getErrorMsg(const char *funcName, int err, const char *errMsg);
char *getNetcdfErrorMsg(const char *funcName, int err);

int getVarId(int ncid, char *varname, int *varid, char **errmsg);
int getGroupAndVarIds(int ncid, char *fullvarname, int *grpid,
  int *varid, char **errmsg);
int createGroup(int ncid, char *fullvarname, int *grpid, char **varname,
  char **errmsg);

int getFillValue(int ncid, int varid, int *fillMode, double *dFillValue,
  char **errmsg);

#endif
