#ifndef _SCINETCDF_HL
#define _SCINETCDF_HL

#include "scinetcdf_hl_types.h"

#ifdef WIN32
#  ifdef DLL_SCINETCDF_HL_EXPORTS
#    define SCINETCDF_HL_API __declspec(dllexport)
#  else
#    define SCINETCDF_HL_API __declspec(dllimport)
#  endif
#else
#  define SCINETCDF_HL_API
#endif

SCINETCDF_HL_API void imp_nccreate(char *filename, char *fullvarname, char **errmsg, ...);
SCINETCDF_HL_API void imp_ncwrite(char *filename, char *fullvarname, st_data data, char **errmsg, size_t *start, ptrdiff_t *stride);
SCINETCDF_HL_API void imp_ncread(char *filename, char *fullvarname, st_data *data, char **errmsg, size_t *start, size_t *count, ptrdiff_t *stride);
SCINETCDF_HL_API void imp_ncwriteatt(char *filename, char *location, char *attname, st_attdata attdata, char **errmsg);
SCINETCDF_HL_API void imp_ncreadatt(char *source, char *location, char *attname, st_attdata *attdata, char **errmsg);

#endif
